# uses open jdk
FROM openjdk:11.0.8

# variables for build
ENV POSTGRES_URL=jdbc:postgresql://postgres-db-server-fortza.postgres.database.azure.com:5432/trainings
ENV POSTGRES_USER=adminfortza@postgres-db-server-fortza
ENV POSTGRES_PASSWORD=fortzosul10!
#ENV env_dbEndpoint=${dbEndpoint} env_dbUser=${dbUser} env_dbPassword=${dbPassword}

# copy the artifacts we need from the first stage and discard the rest
COPY ./target/training-management*.jar /project.jar

# exposing port 5432 for app
EXPOSE 5432
EXPOSE 8080

# set the startup command to execute the jar

#ENTRYPOINT [ "sh", "-c", "java -DPOSTGRES_URL=${env_dbEndpoint} -DPOSTGRES_USER=${env_dbUser} -DPOSTGRES_PASSWORD=${env_dbPassword} -jar -Dspring.profiles.active=prod project.jar" ]
