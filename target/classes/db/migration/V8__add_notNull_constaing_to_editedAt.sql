SET search_path TO trainings_schema;
update trainings set edited_at = '2020-09-01' where id=1;
update trainings set edited_at = '2020-08-01' where id=2;
update trainings set edited_at = '2020-07-01' where id=3;

alter table trainings
    alter column edited_at set  NOT NULL;