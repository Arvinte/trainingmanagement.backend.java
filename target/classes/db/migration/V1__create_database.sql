SET search_path TO trainings_schema;
CREATE TABLE disciplines(
id bigint,
name varchar(50) NOT NULL,
PRIMARY KEY(id)
);
CREATE TABLE employees(
id bigint,
first_name varchar(50) NOT NULL,
last_name varchar(50) NOT NULL,
email varchar(50) NOT NULL UNIQUE,
discipline_id bigint,
PRIMARY KEY(id),
FOREIGN KEY(discipline_id) REFERENCES disciplines(id)
);
CREATE TABLE roles(
id bigint,
name varchar(50) NOT NULL,
PRIMARY KEY(id)
);
CREATE TABLE employees_roles(
employee_id bigint,
role_id bigint,
PRIMARY KEY(employee_id, role_id),
FOREIGN KEY(employee_id) REFERENCES employees(id),
FOREIGN KEY(role_id) REFERENCES roles(id)
);
CREATE TABLE trainings(
id bigint,
name varchar(50) NOT NULL,
location varchar(100) NOT NULL,
area varchar(50) NOT NULL,
start_date TIMESTAMP NOT NULL,
end_date TIMESTAMP NOT NULL,
meeting_url varchar(1000) NOT NULL,
ics bytea,
ics_filename varchar(50),
created_at TIMESTAMP NOT NULL,
edited_at TIMESTAMP ,
is_archived Boolean,
discipline_id bigint,
description varchar(1000),
PRIMARY KEY(id),
FOREIGN KEY(discipline_id) REFERENCES disciplines(id)
);
CREATE TABLE trainings_participants(
training_id bigint,
employee_id bigint,
status varchar(50) NOT NULL,
PRIMARY KEY(training_id, employee_id),
FOREIGN KEY(training_id) REFERENCES trainings(id),
FOREIGN KEY(employee_id) REFERENCES employees(id)
);
CREATE TABLE trainings_trainers(
training_id bigint,
employee_id bigint,
PRIMARY KEY(training_id, employee_id),
FOREIGN KEY(training_id) REFERENCES trainings(id),
FOREIGN KEY(employee_id) REFERENCES employees(id)
);
CREATE TABLE skills(
id bigint,
name varchar(50) NOT NULL,
PRIMARY KEY(id)
);
CREATE TABLE trainings_skills(
skill_id bigint,
training_id bigint,
PRIMARY KEY(skill_id, training_id),
FOREIGN KEY(skill_id) REFERENCES skills(id),
FOREIGN KEY(training_id) REFERENCES trainings(id)
);