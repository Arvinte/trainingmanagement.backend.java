SET search_path TO trainings_schema;
alter table trainings
    alter column end_date drop not null;
alter table trainings
    alter column start_date drop not null;
alter table trainings
    alter column location drop not null;
alter table trainings
    alter column meeting_url drop not null;
alter table trainings
    alter column name TYPE varchar(200);
