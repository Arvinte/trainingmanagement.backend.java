SET search_path TO trainings_schema;
insert into disciplines(id,name) values (nextval('seq_discipline'),'Applications Management');
insert into disciplines(id,name) values (nextval('seq_discipline'),'Creative Services');
insert into disciplines(id,name) values (nextval('seq_discipline'),'Business Analysis');
insert into disciplines(id,name) values (nextval('seq_discipline'),'Project Management');
insert into disciplines(id,name) values (nextval('seq_discipline'),'Data science');
insert into disciplines(id,name) values (nextval('seq_discipline'),'PDR');

insert into skills(id,name) values (nextval('seq_skill'),'.NET ASP');
insert into skills(id,name) values (nextval('seq_skill'),'REACT');
insert into skills(id,name) values (nextval('seq_skill'),'SCRUM');
insert into skills(id,name) values (nextval('seq_skill'),'AGILE');
insert into skills(id,name) values (nextval('seq_skill'),'AGILE SIZING');
insert into skills(id,name) values (nextval('seq_skill'),'SIZING FOR DISCOVERY TEAMS');
insert into skills(id,name) values (nextval('seq_skill'),'SECURITY STANDARDS');
insert into skills(id,name) values (nextval('seq_skill'),'SECURITY & AUDITS');
insert into skills(id,name) values (nextval('seq_skill'),'UI/UX');

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 1','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',1);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 2','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 2);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 3','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 1);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 4','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',5);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 5','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 3);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 6','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 4);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 7','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',5);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 8','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 2);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 9','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 7);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 10','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',8);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 11','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 9);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 12','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 6);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 13','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',4);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 14','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 5);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 15','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 2);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 16','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',6);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 17','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 9);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 18','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 8);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 19','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',7);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 20','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 3);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 21','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 2);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 22','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',5);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 23','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 5);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 24','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 7);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 25','On-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',8);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 26','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02', false, 'description', 3);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 27','On-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 7);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 28','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',2);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 29','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 9);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 30','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 6);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 31','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',3);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 32','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 9);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 33','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 2);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 34','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',3);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 35','On-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 5);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 36','On-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 3);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 37','On-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',8);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 38','On-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 8);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 39','On-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 7);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 40','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',9);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 41','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 4);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 42','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 9);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 43','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',4);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 44','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 3);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 45','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 3);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 46','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 8);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 47','Off-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',8);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 48','Off-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 9);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 49','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 1);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 50','Off-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',6);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 51','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 6);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 52','Of-site','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'blabla', 6);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 53','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false, 'description',6);

insert into trainings(id,name,location,start_date,end_date,meeting_url,ics, ics_filename,created_at,edited_at,is_archived,description,discipline_id)
values(nextval('seq_training'),'Training 54','Online','2020-10-01','2020-11-02','https://random_meeting_url.ro',NULL,'no_filename','2020-09-01','2020-11-02',false,'description', 5);

insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume100','Nume100','email100@example.com',1);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume200','Nume200','email200@example.com',2);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume300','Nume300','email300@example.com',3);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume4','Nume4','email4@example.com',4);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume5','Nume5','email5@example.com',5);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume6','Nume6','email6@example.com',6);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume7','Nume7','email7@example.com',7);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume8','Nume8','email8@example.com',8);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume9','Nume9','email9@example.com',9);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume10','Nume10','email10@example.com',1);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume11','Nume11','email11@example.com',1);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume12','Nume12','email12@example.com',2);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume13','Nume13','email13@example.com',3);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume14','Nume14','email14@example.com',4);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume15','Nume15','email15@example.com',5);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume16','Nume16','email16@example.com',6);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume17','Nume17','email17@example.com',7);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume18','Nume18','email18@example.com',8);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume19','Nume19','email19@example.com',9);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume20','Nume20','email20@example.com',5);
insert into employees(id,first_name,last_name,email,discipline_id) values(nextval('seq_employee'),'Prenume21','Nume21','email21@example.com',6);

insert into employees_roles(employee_id,role_id) values(4,2);
insert into employees_roles(employee_id,role_id) values(5,2);
insert into employees_roles(employee_id,role_id) values(6,2);
insert into employees_roles(employee_id,role_id) values(7,2);
insert into employees_roles(employee_id,role_id) values(8,1);
insert into employees_roles(employee_id,role_id) values(9,2);
insert into employees_roles(employee_id,role_id) values(10,2);
insert into employees_roles(employee_id,role_id) values(11,2);
insert into employees_roles(employee_id,role_id) values(12,1);
insert into employees_roles(employee_id,role_id) values(13,2);
insert into employees_roles(employee_id,role_id) values(14,2);
insert into employees_roles(employee_id,role_id) values(15,1);
insert into employees_roles(employee_id,role_id) values(16,2);
insert into employees_roles(employee_id,role_id) values(17,2);
insert into employees_roles(employee_id,role_id) values(18,2);
insert into employees_roles(employee_id,role_id) values(19,2);
insert into employees_roles(employee_id,role_id) values(20,1);
insert into employees_roles(employee_id,role_id) values(21,1);

insert into trainings_participants(employee_id,training_id,status) values(2,1,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(3,1,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(4,1,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(5,1,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(6,1,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(7,1,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(8,2,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(9,2,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(10,2,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(11,2,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(12,2,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(13,2,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(14,2,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(15,2,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(16,2,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(17,2,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(18,2,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(19,2,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(20,2,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(21,2,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(1,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(2,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(3,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(4,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(5,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(6,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(7,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(8,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(9,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(10,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(11,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(12,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(13,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(14,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(15,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(16,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(17,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(18,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(19,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(20,4,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(21,4,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(1,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(2,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(4,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(5,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(6,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(7,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(8,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(9,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(10,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(11,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(12,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(13,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(14,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(15,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(16,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(17,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(18,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(19,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(20,3,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(21,3,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(1,5,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(2,5,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(3,5,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(4,6,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(5,7,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(6,7,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(7,7,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(8,8,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(9,9,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(10,22,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(11,23,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(12,24,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(13,35,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(14,35,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(15,35,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(16,35,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(17,35,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(18,35,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(19,35,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(20,35,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(21,35,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(1,44,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(2,44,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(3,45,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(4,46,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(5,46,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(6,46,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(7,46,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(8,46,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(9,46,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(10,47,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(11,47,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(12,47,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(13,48,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(14,48,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(15,49,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(16,50,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(17,51,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(18,51,'Declined');
insert into trainings_participants(employee_id,training_id,status) values(19,52,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(20,53,'Confirmed');
insert into trainings_participants(employee_id,training_id,status) values(21,53,'Declined');

insert into trainings_skills(training_id,skill_id) values (2,8);
insert into trainings_skills(training_id,skill_id) values (3,5);
insert into trainings_skills(training_id,skill_id) values (4,2);
insert into trainings_skills(training_id,skill_id) values (5,7);
insert into trainings_skills(training_id,skill_id) values (6,3);
insert into trainings_skills(training_id,skill_id) values (7,1);
insert into trainings_skills(training_id,skill_id) values (8,5);
insert into trainings_skills(training_id,skill_id) values (9,2);
insert into trainings_skills(training_id,skill_id) values (10,3);
insert into trainings_skills(training_id,skill_id) values (10,1);
insert into trainings_skills(training_id,skill_id) values (11,6);
insert into trainings_skills(training_id,skill_id) values (12,2);
insert into trainings_skills(training_id,skill_id) values (13,3);
insert into trainings_skills(training_id,skill_id) values (14,1);
insert into trainings_skills(training_id,skill_id) values (15,2);
insert into trainings_skills(training_id,skill_id) values (16,2);
insert into trainings_skills(training_id,skill_id) values (16,3);
insert into trainings_skills(training_id,skill_id) values (17,1);
insert into trainings_skills(training_id,skill_id) values (17,2);
insert into trainings_skills(training_id,skill_id) values (18,2);
insert into trainings_skills(training_id,skill_id) values (19,3);
insert into trainings_skills(training_id,skill_id) values (20,1);
insert into trainings_skills(training_id,skill_id) values (21,2);
insert into trainings_skills(training_id,skill_id) values (22,6);
insert into trainings_skills(training_id,skill_id) values (23,3);
insert into trainings_skills(training_id,skill_id) values (24,8);
insert into trainings_skills(training_id,skill_id) values (25,8);
insert into trainings_skills(training_id,skill_id) values (25,6);
insert into trainings_skills(training_id,skill_id) values (26,8);
insert into trainings_skills(training_id,skill_id) values (27,8);
insert into trainings_skills(training_id,skill_id) values (28,7);
insert into trainings_skills(training_id,skill_id) values (29,2);
insert into trainings_skills(training_id,skill_id) values (30,3);
insert into trainings_skills(training_id,skill_id) values (31,7);
insert into trainings_skills(training_id,skill_id) values (31,2);
insert into trainings_skills(training_id,skill_id) values (32,5);
insert into trainings_skills(training_id,skill_id) values (33,4);
insert into trainings_skills(training_id,skill_id) values (34,1);
insert into trainings_skills(training_id,skill_id) values (35,2);
insert into trainings_skills(training_id,skill_id) values (35,3);
insert into trainings_skills(training_id,skill_id) values (36,3);
insert into trainings_skills(training_id,skill_id) values (37,1);
insert into trainings_skills(training_id,skill_id) values (38,2);
insert into trainings_skills(training_id,skill_id) values (39,2);
insert into trainings_skills(training_id,skill_id) values (40,3);
insert into trainings_skills(training_id,skill_id) values (41,1);
insert into trainings_skills(training_id,skill_id) values (41,7);
insert into trainings_skills(training_id,skill_id) values (42,4);
insert into trainings_skills(training_id,skill_id) values (43,3);
insert into trainings_skills(training_id,skill_id) values (44,1);
insert into trainings_skills(training_id,skill_id) values (45,2);
insert into trainings_skills(training_id,skill_id) values (45,7);
insert into trainings_skills(training_id,skill_id) values (46,8);
insert into trainings_skills(training_id,skill_id) values (47,9);
insert into trainings_skills(training_id,skill_id) values (48,7);
insert into trainings_skills(training_id,skill_id) values (49,2);
insert into trainings_skills(training_id,skill_id) values (50,3);
insert into trainings_skills(training_id,skill_id) values (51,7);
insert into trainings_skills(training_id,skill_id) values (52,8);
insert into trainings_skills(training_id,skill_id) values (53,2);
insert into trainings_skills(training_id,skill_id) values (54,9);

insert into trainings_trainers(training_id, employee_id) values(3,3);
insert into trainings_trainers(training_id, employee_id) values(3,4);
insert into trainings_trainers(training_id, employee_id) values(4,20);
insert into trainings_trainers(training_id, employee_id) values(5,20);
insert into trainings_trainers(training_id, employee_id) values(6,13);
insert into trainings_trainers(training_id, employee_id) values(7,11);
insert into trainings_trainers(training_id, employee_id) values(7,12);
insert into trainings_trainers(training_id, employee_id) values(7,17);
insert into trainings_trainers(training_id, employee_id) values(8,15);
insert into trainings_trainers(training_id, employee_id) values(9,3);
insert into trainings_trainers(training_id, employee_id) values(10,7);
insert into trainings_trainers(training_id, employee_id) values(10,2);
insert into trainings_trainers(training_id, employee_id) values(11,1);
insert into trainings_trainers(training_id, employee_id) values(12,4);
insert into trainings_trainers(training_id, employee_id) values(13,6);
insert into trainings_trainers(training_id, employee_id) values(14,6);
insert into trainings_trainers(training_id, employee_id) values(15,6);
insert into trainings_trainers(training_id, employee_id) values(16,12);
insert into trainings_trainers(training_id, employee_id) values(16,20);
insert into trainings_trainers(training_id, employee_id) values(17,20);
insert into trainings_trainers(training_id, employee_id) values(18,13);
insert into trainings_trainers(training_id, employee_id) values(19,11);
insert into trainings_trainers(training_id, employee_id) values(20,20);
insert into trainings_trainers(training_id, employee_id) values(21,4);
insert into trainings_trainers(training_id, employee_id) values(21,11);
insert into trainings_trainers(training_id, employee_id) values(22,1);
insert into trainings_trainers(training_id, employee_id) values(23,2);
insert into trainings_trainers(training_id, employee_id) values(24,4);
insert into trainings_trainers(training_id, employee_id) values(25,2);
insert into trainings_trainers(training_id, employee_id) values(25,4);
insert into trainings_trainers(training_id, employee_id) values(25,20);
insert into trainings_trainers(training_id, employee_id) values(26,11);
insert into trainings_trainers(training_id, employee_id) values(27,12);
insert into trainings_trainers(training_id, employee_id) values(28,20);
insert into trainings_trainers(training_id, employee_id) values(29,13);
insert into trainings_trainers(training_id, employee_id) values(30,6);
insert into trainings_trainers(training_id, employee_id) values(30,7);
insert into trainings_trainers(training_id, employee_id) values(31,7);
insert into trainings_trainers(training_id, employee_id) values(32,15);
insert into trainings_trainers(training_id, employee_id) values(33,12);
insert into trainings_trainers(training_id, employee_id) values(34,6);
insert into trainings_trainers(training_id, employee_id) values(35,6);
insert into trainings_trainers(training_id, employee_id) values(36,6);
insert into trainings_trainers(training_id, employee_id) values(36,11);
insert into trainings_trainers(training_id, employee_id) values(37,11);
insert into trainings_trainers(training_id, employee_id) values(38,12);
insert into trainings_trainers(training_id, employee_id) values(39,15);
insert into trainings_trainers(training_id, employee_id) values(40,20);
insert into trainings_trainers(training_id, employee_id) values(41,20);
insert into trainings_trainers(training_id, employee_id) values(41,15);
insert into trainings_trainers(training_id, employee_id) values(42,20);
insert into trainings_trainers(training_id, employee_id) values(43,11);
insert into trainings_trainers(training_id, employee_id) values(44,11);
insert into trainings_trainers(training_id, employee_id) values(45,11);
insert into trainings_trainers(training_id, employee_id) values(45,12);
insert into trainings_trainers(training_id, employee_id) values(45,1);
insert into trainings_trainers(training_id, employee_id) values(46,11);
insert into trainings_trainers(training_id, employee_id) values(47,11);
insert into trainings_trainers(training_id, employee_id) values(48,6);
insert into trainings_trainers(training_id, employee_id) values(49,20);
insert into trainings_trainers(training_id, employee_id) values(50,1);
insert into trainings_trainers(training_id, employee_id) values(50,6);
insert into trainings_trainers(training_id, employee_id) values(51,2);
insert into trainings_trainers(training_id, employee_id) values(52,3);
insert into trainings_trainers(training_id, employee_id) values(53,2);
insert into trainings_trainers(training_id, employee_id) values(54,3);