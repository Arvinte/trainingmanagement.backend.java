#!/usr/bin/env sh

set -m

java -jar target/training-management*.jar --spring.config.location=file:src/test/resources/application.properties &
app_process_id=$!

sleep 30

newman run src/test/resources/postman_collection.json | tee newman.logs

kill -9 ${app_process_id}

if grep AssertionError newman.logs; then
	echo "There are test failures"
	exit 1
else
    echo "All tests passed"
fi
