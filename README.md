# Database
-create the database named "trainings"

-for the created database, create schema named "trainings_schema"

-add in the System variables the following values

  | Environment variable | Value | Description |
  | :---:         |     :---:      |          :---: |
  | POSTGRES_URL      | jdbc:postgresql://localhost:5432/trainings | Url used to configure the database and Flyway|
  |POSTGRES_USER     | postgres |  Username used to configure the database and Flyway |      
  |POSTGRES_PASSWORD     |your postgres password| Password to configure the database and Flyway|
  |POSTGRES_SCHEMA       |trainings_schema| Database schema|

-the configuration of the postgres database with springboot can be found in application.properties file

# Flyway
-add in pom.xml:

-the flyway configuration can be found in **pom.xml** but also in **application.properties**
    
-in the "resources" folder add "db" folder and in the "db" folder add the "migration" folder. The result will be: resources/db/migration

-in the "migration" folder create sql files with the format: Ex: "V1__create_database.sql" where:

 *"V" - prefix
 
 *"1" - version
 
 *"__" - separator
 
 *"create_database" - description
 
 *".sql" - sufix
 
 
-to run the migrations execute in terminal "mvn flyway:migrate"

-the database will contain a table "flyway_schema_history" where all the migrations will be added
 