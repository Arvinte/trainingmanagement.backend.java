CREATE TABLE trainings_schema.disciplines(
id bigint,
name varchar(50) NOT NULL,
PRIMARY KEY(id)
);
CREATE TABLE trainings_schema.employees(
id bigint,
first_name varchar(50) NOT NULL,
last_name varchar(50) NOT NULL,
email varchar(50) NOT NULL UNIQUE,
discipline_id bigint,
PRIMARY KEY(id),
FOREIGN KEY(discipline_id) REFERENCES trainings_schema.disciplines(id)
);
CREATE TABLE trainings_schema.roles(
id bigint,
name varchar(50) NOT NULL,
PRIMARY KEY(id)
);
CREATE TABLE trainings_schema.employees_roles(
employee_id bigint,
role_id bigint,
PRIMARY KEY(employee_id, role_id),
FOREIGN KEY(employee_id) REFERENCES trainings_schema.employees(id),
FOREIGN KEY(role_id) REFERENCES trainings_schema.roles(id)
);
CREATE TABLE trainings_schema.trainings(
id bigint,
name varchar(50) NOT NULL,
location varchar(100) NOT NULL,
area varchar(50) NOT NULL,
start_date TIMESTAMP NOT NULL,
end_date TIMESTAMP NOT NULL,
meeting_url varchar(1000) NOT NULL,
ics bytea,
ics_filename varchar(50),
created_at TIMESTAMP NOT NULL,
edited_at TIMESTAMP ,
is_archived Boolean,
discipline_id bigint,
description varchar(1000),
PRIMARY KEY(id),
FOREIGN KEY(discipline_id) REFERENCES trainings_schema.disciplines(id)
);
CREATE TABLE trainings_schema.trainings_participants(
training_id bigint,
employee_id bigint,
status varchar(50) NOT NULL,
PRIMARY KEY(training_id, employee_id),
FOREIGN KEY(training_id) REFERENCES trainings_schema.trainings(id),
FOREIGN KEY(employee_id) REFERENCES trainings_schema.employees(id)
);
CREATE TABLE trainings_schema.trainings_trainers(
training_id bigint,
employee_id bigint,
PRIMARY KEY(training_id, employee_id),
FOREIGN KEY(training_id) REFERENCES trainings_schema.trainings(id),
FOREIGN KEY(employee_id) REFERENCES trainings_schema.employees(id)
);
CREATE TABLE trainings_schema.skills(
id bigint,
name varchar(50) NOT NULL,
PRIMARY KEY(id)
);
CREATE TABLE trainings_schema.trainings_skills(
skill_id bigint,
training_id bigint,
PRIMARY KEY(skill_id, training_id),
FOREIGN KEY(skill_id) REFERENCES trainings_schema.skills(id),
FOREIGN KEY(training_id) REFERENCES trainings_schema.trainings(id)
);