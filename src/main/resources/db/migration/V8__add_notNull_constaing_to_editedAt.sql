update trainings_schema.trainings set edited_at = '2020-09-01' where id=1;
update trainings_schema.trainings set edited_at = '2020-08-01' where id=2;
update trainings_schema.trainings set edited_at = '2020-07-01' where id=3;

alter table trainings_schema.trainings
    alter column edited_at set  NOT NULL;