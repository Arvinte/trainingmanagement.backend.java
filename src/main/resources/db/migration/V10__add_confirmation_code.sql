alter table trainings_schema.trainings_participants
    add invitation_code uuid;

alter table trainings_schema.trainings_participants
    add modification_date timestamp;

UPDATE trainings_schema.trainings_participants
set modification_date=now(),
    invitation_code='a81bc81b-dead-4e5d-abff-90865d1e13b1'
where employee_id = 1;

UPDATE trainings_schema.trainings_participants
set modification_date=now(),
    invitation_code='a81bc81b-dead-4e5d-abff-90865d1e13b1'
where employee_id = 2;


UPDATE trainings_schema.trainings_participants
set modification_date=now(),
    invitation_code='a81bc81b-dead-4e5d-abff-90865d1e13b1'
where employee_id = 3;

UPDATE trainings_schema.employees
set first_name='Preenumee',
    last_name='Naaaame'
where id = 1;


UPDATE trainings_schema.employees
set first_name='Preenumee',
    last_name='Naaaamee'
where id = 2;


UPDATE trainings_schema.employees
set first_name='Preenumee',
    last_name='Naaaameere'
where id = 3;

