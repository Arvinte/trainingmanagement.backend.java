insert into disciplines(id, name)
values (nextval('seq_discipline'), 'Development');
insert into disciplines(id, name)
values (nextval('seq_discipline'), 'Testing');
insert into disciplines(id, name)
values (nextval('seq_discipline'), 'Architecture');

insert into roles(id, name)
values (nextval('seq_role'), 'PDR');
insert into roles(id, name)
values (nextval('seq_role'), 'NORMAL');

insert into skills(id, name)
values (nextval('seq_skill'), 'JAVA');
insert into skills(id, name)
values (nextval('seq_skill'), 'SPRING');
insert into skills(id, name)
values (nextval('seq_skill'), 'C#');
insert into skills(id, name)
values (nextval('seq_skill'), 'ANGULAR');

insert into trainings(id, name, location, area, start_date, end_date, meeting_url, ics, ics_filename, created_at,
                      edited_at, is_archived, description, discipline_id)
values (nextval('seq_training'), 'First Java training', 'Remote', 'Iasi', '2020-10-01', '2020-11-02',
        'https://random_meeting_url.ro', NULL, 'no_filename', '2020-09-01', NULL, false, 'blabla', 1);

insert into trainings(id, name, location, area, start_date, end_date, meeting_url, ics, ics_filename, created_at,
                      edited_at, is_archived, description, discipline_id)
values (nextval('seq_training'), 'First C# training', 'Remote', 'Iasi', '2020-10-01', '2020-11-02',
        'https://random_meeting_url.ro', NULL, 'no_filename', '2020-09-01', NULL, false, 'description', 2);

insert into trainings(id, name, location, area, start_date, end_date, meeting_url, ics, ics_filename, created_at,
                      edited_at, is_archived, description, discipline_id)
values (nextval('seq_training'), 'First RELATIONS training', 'Remote', 'Iasi', '2020-10-01', '2020-11-02','https://random_meeting_url.ro', NULL, 'no_filename', '2020-09-01', NULL, false, 'description', 3);

insert into employees(id, first_name, last_name, email, discipline_id)
values (nextval('seq_employee'), 'Prenume1', 'Nume1', 'email1@example.com', 1);
insert into employees(id, first_name, last_name, email, discipline_id)
values (nextval('seq_employee'), 'Prenume2', 'Nume2', 'email2@example.com', 2);
insert into employees(id, first_name, last_name, email, discipline_id)
values (nextval('seq_employee'), 'Prenume3', 'Nume3', 'email3@example.com', 3);

insert into trainings_schema.employees_roles(employee_id, role_id) values (1, 1);
insert into trainings_schema.employees_roles(employee_id, role_id) values (2, 1);
insert into trainings_schema.employees_roles(employee_id, role_id) values (3, 2);

insert into trainings_participants(training_id, employee_id, status) values (1, 1, 'Confirmed');
insert into trainings_participants(training_id, employee_id, status) values (2, 2, 'Confirmed');
insert into trainings_participants(training_id, employee_id, status) values (3, 3, 'Declined');

insert into trainings_skills(skill_id, training_id) values (1, 1);
insert into trainings_skills(skill_id, training_id) values (1, 2);
insert into trainings_skills(skill_id, training_id) values (2, 2);
insert into trainings_skills(skill_id, training_id) values (3, 3);

insert into trainings_schema.trainings_trainers(training_id, employee_id) values (1, 1);
insert into trainings_schema.trainings_trainers(training_id, employee_id) values (2, 2);
insert into trainings_schema.trainings_trainers(training_id, employee_id) values (1, 3);