alter table trainings_schema.trainings
    alter column end_date drop not null;
alter table trainings_schema.trainings
    alter column start_date drop not null;
alter table trainings_schema.trainings
    alter column location drop not null;
alter table trainings_schema.trainings
    alter column meeting_url drop not null;
alter table trainings_schema.trainings
    alter column name TYPE varchar(200);
