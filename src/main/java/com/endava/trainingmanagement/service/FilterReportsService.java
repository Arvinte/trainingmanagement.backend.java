package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.dto.FilterTrainingReportResultDTO;
import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.model.TrainingParticipant;
import com.endava.trainingmanagement.repository.TrainingRepository;
import com.endava.trainingmanagement.utils.ReportFilter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
@Service
public class FilterReportsService {
    private final TrainingRepository trainingRepository;
    public static final int PAGE_SIZE = 10;

    public List<TrainingParticipant> getParticipantsWithACertainFirstNameOrLastName(List<String> namesToLookFor, List<TrainingParticipant> participants) {
        return participants.stream()
                .filter(participant -> (namesToLookFor.contains(participant.getEmployee().getFirstName()) || (namesToLookFor.contains(participant.getEmployee().getLastName()))))
                .collect(Collectors.toList());
    }

    public List<TrainingParticipant> getParticipantsWithACertainStatus(List<String> status, List<TrainingParticipant> participants) {
        return participants.stream()
                .filter(participant -> status.contains(participant.getStatus().getValue()))
                .collect(Collectors.toList());
    }

    public List<FilterTrainingReportResultDTO> mapTrainingToFilterTrainingReportResultDto(ReportFilter filter, Training training) {
        List<FilterTrainingReportResultDTO> filterTrainingReportResultDTOS = new LinkedList<>();

        List<TrainingParticipant> participants = training.getParticipants().stream().collect(Collectors.toList());

        if (filter.getTraineeName() != null) {
            participants = getParticipantsWithACertainFirstNameOrLastName(filter.getTraineeName(), participants);
        }

        if (filter.getTraineeStatus() != null) {
            participants = getParticipantsWithACertainStatus(filter.getTraineeStatus(), participants);
        }
        for (TrainingParticipant trp : participants) {
            filterTrainingReportResultDTOS.add(this.buildFilterResultFromTrainingParticipant(trp, training));
        }

        return filterTrainingReportResultDTOS;
    }

    public List<FilterTrainingReportResultDTO> getTrainingsForReportPaginated(Long pageNumber, ReportFilter reportFilter) {
        return this.getTrainingsForReport(reportFilter).stream()
                .skip((pageNumber - 1) * PAGE_SIZE)
                .limit(PAGE_SIZE)
                .collect(Collectors.toList());
    }

    public List<FilterTrainingReportResultDTO> getTrainingsForReport(ReportFilter reportFilter) {
        reportFilter.toLowerCase();
        List<Training> trainings = this.trainingRepository.getTrainingsForReport(reportFilter);

        List<FilterTrainingReportResultDTO> trainingReportResultDTOS = new LinkedList<>();

        for (Training training : trainings) {
            trainingReportResultDTOS.addAll(this.mapTrainingToFilterTrainingReportResultDto(reportFilter, training));
        }

        return trainingReportResultDTOS;
    }

    public int getReportSize(ReportFilter reportFilter) {
        return getTrainingsForReport(reportFilter).size();
    }

    private FilterTrainingReportResultDTO buildFilterResultFromTrainingParticipant(TrainingParticipant trp, Training training) {
        return FilterTrainingReportResultDTO.builder()
                .endDate(trp.getTraining().getEndDate().toString())
                .startDate(trp.getTraining().getStartDate().toString())
                .discipline(trp.getTraining().getDiscipline().getName())
                .trainee(trp.getEmployee().getFirstName() + " " + trp.getEmployee().getLastName())
                .trainingName(training.getName())
                .traineeStatus(trp.getStatus().toString())
                .trainers(training.getTrainers().stream().map(employee -> employee.getLastName() + " " + employee.getFirstName()).collect(Collectors.toList()))
                .skills(training.getSkills().stream().map(skill -> skill.getName()).collect(Collectors.toList()))
                .build();
    }
}
