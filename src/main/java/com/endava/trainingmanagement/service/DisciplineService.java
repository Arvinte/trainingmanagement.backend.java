package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.dto.DisciplineDto;
import com.endava.trainingmanagement.mapper.DisciplineMapper;
import com.endava.trainingmanagement.repository.DisciplineRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class DisciplineService {
    private final DisciplineRepository disciplineRepository;

    public List<DisciplineDto> getAll() {
        return disciplineRepository.findAll().stream()
                .map(DisciplineMapper::mapToDisciplineDto)
                .collect(Collectors.toList());
    }
}
