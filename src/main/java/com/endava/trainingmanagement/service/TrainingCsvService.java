package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.dto.FilterTrainingReportResultDTO;
import com.endava.trainingmanagement.exceptions.EntityNotFoundException;
import com.endava.trainingmanagement.model.Employee;
import com.endava.trainingmanagement.model.Skill;
import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.model.TrainingParticipant;
import com.endava.trainingmanagement.repository.TrainingRepository;
import com.endava.trainingmanagement.utils.Filter;
import com.endava.trainingmanagement.utils.ReportFilter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
@Service
public class TrainingCsvService {
    private final TrainingRepository trainingRepository;
    private final FilterReportsService filterReportsService;
    private final TrainingService trainingService;

    public ResponseEntity<ByteArrayResource> getTrainingParticipantsCsv(Long trainingId) {
        Training training = trainingRepository
                .findById(trainingId).orElseThrow(() -> new EntityNotFoundException("Training", trainingId));
        Set<TrainingParticipant> participants = training.getParticipants();

        byte[] csv = ("First Name,Last Name,Status\n" + participants.stream()
                .map(this::participantsMapNameFunction)
                .collect(Collectors.joining())).getBytes();

        ByteArrayResource resource = new ByteArrayResource(csv);

        String contentDisposition = String.format("attachment; filename=\"%s.csv\"", training.getName());

        return ResponseEntity.ok()
                .header("Content-Disposition", contentDisposition)
                .contentLength(csv.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    public ResponseEntity<ByteArrayResource> getEmployeesReportFromFilters(ReportFilter reportFilter) {
        return getEmployeesReport(filterReportsService.getTrainingsForReport(reportFilter));
    }

    public ResponseEntity<ByteArrayResource> getTrainingsReportFromFilters(Filter filter) {
        return getTrainingsReport(trainingService.filterTrainings(filter));
    }

    private ResponseEntity<ByteArrayResource> getEmployeesReport(List<FilterTrainingReportResultDTO> employees) {
        byte[] csv = ("Training name,Trainer,Trainee,Trainee Status,Skills,Discipline,Start,End\n" + employees.stream()
                .map(this::getEmployeeReportCsvLine)
                .collect(Collectors.joining())).getBytes();

        ByteArrayResource resource = new ByteArrayResource(csv);

        String contentDisposition = String.format("attachment; filename=\"%s.csv\"", "report");

        return ResponseEntity.ok()
                .header("Content-Disposition", contentDisposition)
                .contentLength(csv.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    private String getEmployeeReportCsvLine(FilterTrainingReportResultDTO result) {
        return String.format("%s,%s,%s,%s,%s,%s\n",
                result.getTrainingName(),
                String.join(" / ", result.getTrainers()),
                result.getTrainee(),
                String.join(" / ", result.getSkills()),
                result.getDiscipline(),
                result.getStartDate(),
                result.getEndDate());
    }

    private ResponseEntity<ByteArrayResource> getTrainingsReport(List<Training> trainings) {
        byte[] csv = ("Training name,Location,Trainer,Skills,Discipline,Start,End\n" + trainings.stream()
                .map(this::getTrainingReportCsvLine)
                .collect(Collectors.joining())).getBytes();

        ByteArrayResource resource = new ByteArrayResource(csv);

        String contentDisposition = String.format("attachment; filename=\"%s.csv\"", "report");

        return ResponseEntity.ok()
                .header("Content-Disposition", contentDisposition)
                .contentLength(csv.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    private String getTrainingReportCsvLine(Training training) {
        return String.format("%s,%s,%s,%s,%s,%s,%s\n",
                training.getName(),
                training.getLocation() != null ? training.getLocation() : "",
                training.getTrainers().stream()
                        .map(this::getFullName)
                        .collect(Collectors.joining(" / ")),
                training.getSkills() != null && !training.getSkills().isEmpty() ? training.getSkills().stream()
                        .map(Skill::getName)
                        .collect(Collectors.joining(" / ")) : "",
                training.getDiscipline().getName() != null ? training.getDiscipline().getName() : "",
                training.getStartDate().toString() != null ? training.getStartDate().toString() : "",
                training.getEndDate().toString() != null ? training.getEndDate().toString() : ""
        );
    }

    private String getFullName(Employee employee) {
        return employee.getFirstName() + " " + employee.getLastName();
    }

    private String participantsMapNameFunction(TrainingParticipant participant) {
        Employee employee = participant.getEmployee();
        return String.format("%s,%s,%s\n", employee.getFirstName(), employee.getLastName(), participant.getStatus());
    }
}
