package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.dto.SkillDto;
import com.endava.trainingmanagement.mapper.SkillMapper;
import com.endava.trainingmanagement.repository.SkillRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class SkillService {
    private final SkillRepository skillRepository;

    public List<SkillDto> getAll() {
        return skillRepository.findAll().stream()
                .map(SkillMapper::mapToSkillDto)
                .collect(Collectors.toList());
    }
}
