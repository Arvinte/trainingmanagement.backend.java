package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.constants.StatusInvitation;
import com.endava.trainingmanagement.email.ConfirmationMessage;
import com.endava.trainingmanagement.email.InvitationMessage;
import com.endava.trainingmanagement.exceptions.FailToSendEmail;
import com.endava.trainingmanagement.model.Employee;
import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.model.TrainingParticipant;
import com.endava.trainingmanagement.repository.TrainingRepository;
import com.endava.trainingmanagement.utils.EmailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NotificationService {
    private final EmailSendingService emailService;
    private final TrainingRepository trainingRepository;
    private final String baseUrl;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm a");

    public NotificationService(EmailSendingService emailService, TrainingRepository trainingRepository, @Value("${app.base.url}") String baseUrl) {
        this.emailService = emailService;
        this.trainingRepository = trainingRepository;
        this.baseUrl = baseUrl;
    }

    public void sendDetailsEmail(TrainingParticipant participant) {
        ConfirmationMessage message = new ConfirmationMessage();
        message.setMeetingUrl(participant.getTraining().getMeetingURL());
        message.setNameParticipant(participant.getEmployee().getFirstName());
        message.setTrainer(mapToString(participant.getTraining()));
        message.setTrainingName(participant.getTraining().getName());
        if (participant.getTraining().getStartDate() != null) {
            message.setDateOfTraining(mapDateToString(participant.getTraining().getStartDate()));
        } else {
            message.setDateOfTraining("TBA");
        }
        try {
            byte[] icsFile = participant.getTraining().getIcs();
            if (icsFile == null) {
                emailService.sendEmail(participant.getEmployee().getEmail(), message);
            } else {
                emailService.sendEmail(participant.getEmployee().getEmail(), message, participant.getTraining().getIcs(), participant.getTraining().getName());
            }
        } catch (MessagingException exception) {
            throw new FailToSendEmail(List.of(participant.getEmployee().getEmail()));
        }
    }


    public List<String> sendInvitationForParticipants(Training training) {
        return sendInvitationForParticipants(training, false);
    }

    public List<String> sendInvitationForParticipants(Training training, boolean update) {
        log.debug(String.format("Sending %d invitations.", training.getParticipants().size()));
        InvitationMessage templateInvitation = EmailUtils.getTemplateInvitationEmail(training, update);

        List<String> failToSend = new ArrayList<>();
        for (TrainingParticipant participant : training.getParticipants()) {
            try {
                UUID invitationCode = sendInvitation(templateInvitation.clone(), participant.getEmployee());
                participant.setStatus(StatusInvitation.Invited);
                participant.setModificationDate(Timestamp.from(Instant.now()));
                participant.setInvitationCode(invitationCode);
            } catch (MessagingException | MailSendException exception) {
                log.warn(String.format("Fail to send email to %s", participant.getEmployee().getId()), exception);
                failToSend.add(participant.getEmployee().getEmail());
                participant.setStatus(StatusInvitation.Fail);
            }
        }
        trainingRepository.save(training);
        return failToSend;
    }

    private UUID sendInvitation(InvitationMessage invitationMessage, Employee employee) throws MessagingException {
        UUID invitationCode = UUID.randomUUID();
        invitationMessage.setName(employee.getFirstName());
        invitationMessage.setConfirmationUrl(EmailUtils.createUrlConfirmation(invitationCode, baseUrl));
        emailService.sendEmail(employee.getEmail(), invitationMessage);
        return invitationCode;
    }

    private String mapDateToString(Timestamp time) {
        LocalDateTime localTime = time.toLocalDateTime();
        return localTime.format(formatter);
    }

    private String mapToString(Training training) {
        return training.getTrainers().stream()
                .map(e -> e.getFirstName() + " " + e.getLastName())
                .collect(Collectors.joining(","));
    }


}
