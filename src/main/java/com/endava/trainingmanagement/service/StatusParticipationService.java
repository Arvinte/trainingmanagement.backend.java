package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.constants.StatusInvitation;
import com.endava.trainingmanagement.model.TrainingParticipant;
import com.endava.trainingmanagement.repository.TrainingParticipantRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

@Setter
@Service
public class StatusParticipationService {
    private final TrainingParticipantRepository trainingParticipantRepository;
    private final NotificationService notificationService;

    private static final String PATH_CONFIRMATION_IN_WEBSITE = "confirmation";
    private static final String PATH_ERROR_IN_WEBSITE = "error";

    @Value("${app.email.confirmation.time}")
    private long secondsToExpire;
    @Value("${app.email.confirmation.redirect}")
    private String hostUrlRedirect;

    public StatusParticipationService(TrainingParticipantRepository trainingParticipantRepository, NotificationService notificationService) {
        this.trainingParticipantRepository = trainingParticipantRepository;
        this.notificationService = notificationService;
    }

    public void updateStatus(UUID code, HttpServletResponse response) {
        Optional<TrainingParticipant> participant = trainingParticipantRepository.findByInvitationCode(code);
        if (participant.isEmpty()) {
            redirectToAppPage(createUrlRedirect(PATH_ERROR_IN_WEBSITE+ "/" + code.toString()), response);
            return;
        }
        TrainingParticipant trainingParticipant = participant.get();
        if (!isParticipantInvitedAndInvitationNotExpired(trainingParticipant)) {
            redirectToAppPage(createUrlRedirect(trainingParticipant, PATH_ERROR_IN_WEBSITE + "/" + code.toString()), response);
        } else {
            trainingParticipant.setStatus(StatusInvitation.Confirmed);
            trainingParticipantRepository.save(trainingParticipant);
            notificationService.sendDetailsEmail(trainingParticipant);
            redirectToAppPage(createUrlRedirect(trainingParticipant, PATH_CONFIRMATION_IN_WEBSITE + "/" + code.toString()), response);
        }
    }

    private void redirectToAppPage(String path, HttpServletResponse response) {
        response.setHeader("Location", path);
        response.setStatus(HttpServletResponse.SC_FOUND);
    }

    private boolean isParticipantInvitedAndInvitationNotExpired(TrainingParticipant participant) {
        Timestamp expireTime = Timestamp.from(Instant.now().minus(secondsToExpire, ChronoUnit.SECONDS));
        return participant.getStatus().equals(StatusInvitation.Invited) &&
                expireTime.before(participant.getModificationDate());
    }

    private String createUrlRedirect(TrainingParticipant participant, String path) {
        String name = participant.getTraining().getName();
        return String.format("%s/%s?trainingName=%s", hostUrlRedirect, path, URLEncoder.encode(name, StandardCharsets.UTF_8));
    }

    private String createUrlRedirect(String path) {
        return String.format("%s/%s?trainingName=", hostUrlRedirect, path);
    }
}
