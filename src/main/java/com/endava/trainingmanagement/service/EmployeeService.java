package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.dto.EmployeeDto;
import com.endava.trainingmanagement.exceptions.LoginException;
import com.endava.trainingmanagement.exceptions.NotFoundEmployeesException;
import com.endava.trainingmanagement.mapper.EmployeeMapper;
import com.endava.trainingmanagement.model.Employee;
import com.endava.trainingmanagement.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeDto getByEmail(String email) throws LoginException {
        Employee employee = employeeRepository.getEmployeeByEmail(email).orElseThrow(() -> new LoginException(email));
        return EmployeeMapper.mapToEmployeeDto(employee);
    }

    public ResponseEntity<Object> existsEmployees(List<String> employees) {
        List<String> notFoundList = new ArrayList<>();

        employees.forEach(employeeName -> {
            Optional<Employee> employee = employeeRepository.findByFirstNameAndLastName(employeeName);

            if (employee.isEmpty()) {
                notFoundList.add(employeeName);
            }
        });

        if (notFoundList.isEmpty()) {
            return ResponseEntity.ok().build();
        } else {
           throw new NotFoundEmployeesException(notFoundList);
        }
    }
}
