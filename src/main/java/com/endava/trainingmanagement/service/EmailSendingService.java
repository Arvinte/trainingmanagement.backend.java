package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.email.EmailMessage;
import com.endava.trainingmanagement.utils.EmailMapper;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

@Service
@AllArgsConstructor
public class EmailSendingService {
    private final JavaMailSender mailSender;
    private final EmailMapper emailMapper;

    public void sendEmail(String email, EmailMessage message, byte[] data, String filename) throws MessagingException {
        ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(data, "text/calendar");
        send(email, message.getSubject(), message.getAsHtml(emailMapper), byteArrayDataSource, filename);
    }

    @Async
    public void sendEmail(String email, EmailMessage message) throws MessagingException {
        send(email, message.getSubject(), message.getAsHtml(emailMapper), null, null);
    }

    private void send(String email, String subject, String bodyText, DataSource data, String filename) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setTo(email);
        if (data != null)
            helper.addAttachment(filename + ".ics", data);
        helper.setSubject(subject);
        helper.setText(bodyText, true);
        mailSender.send(message);
    }

}
