package com.endava.trainingmanagement.service;

import com.endava.trainingmanagement.constants.MessageConstants;
import com.endava.trainingmanagement.constants.StatusInvitation;
import com.endava.trainingmanagement.dto.DisciplineDto;
import com.endava.trainingmanagement.dto.EmployeeDto;
import com.endava.trainingmanagement.dto.FilterTrainingResultDto;
import com.endava.trainingmanagement.dto.SkillDto;
import com.endava.trainingmanagement.dto.TrainingDto;
import com.endava.trainingmanagement.exceptions.EntityNotFoundException;
import com.endava.trainingmanagement.exceptions.FailToSendEmail;
import com.endava.trainingmanagement.exceptions.InvalidFieldsException;
import com.endava.trainingmanagement.exceptions.InvalidFileException;
import com.endava.trainingmanagement.exceptions.InvalidPageException;
import com.endava.trainingmanagement.mapper.SkillMapper;
import com.endava.trainingmanagement.mapper.TrainingMapper;
import com.endava.trainingmanagement.model.Discipline;
import com.endava.trainingmanagement.model.Employee;
import com.endava.trainingmanagement.model.Skill;
import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.model.TrainingParticipant;
import com.endava.trainingmanagement.repository.DisciplineRepository;
import com.endava.trainingmanagement.repository.EmployeeRepository;
import com.endava.trainingmanagement.repository.SkillRepository;
import com.endava.trainingmanagement.repository.TrainingParticipantRepository;
import com.endava.trainingmanagement.repository.TrainingRepository;

import static com.endava.trainingmanagement.util.StringUtils.capitalize;

import com.endava.trainingmanagement.utils.Filter;
import com.endava.trainingmanagement.validator.TrainingValidator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.endava.trainingmanagement.util.StringUtils.removeExtraWhitespaces;


@AllArgsConstructor
@Slf4j
@Service
public class TrainingService {
    private final TrainingRepository trainingRepository;
    private final SkillRepository skillRepository;
    private final EmployeeRepository employeeRepository;
    private final DisciplineRepository disciplineRepository;
    private final TrainingParticipantRepository trainingParticipantRepository;
    private final NotificationService notificationService;
    public static final int PAGE_SIZE = 10;

    public TrainingDto saveTraining(TrainingDto trainingDto, Optional<MultipartFile> icsFile)
            throws TransactionSystemException, InvalidFieldsException, InvalidFileException {

        TrainingValidator.validateDates(trainingDto);
        TrainingValidator.validateIcsFile(icsFile);

        Training training = mapToTraining(trainingDto, icsFile);

        Instant now = Instant.now();
        training.setCreatedAt(Timestamp.from(now));
        training.setEditedAt(Timestamp.from(now));
        training.setArchived(false);

        Training savedTraining = trainingRepository.save(training);
        Training foundTraining = trainingRepository.getOne(savedTraining.getId());
        foundTraining.setParticipants(getParticipantList(trainingDto, savedTraining));

        Training trainingWithParticipants = trainingRepository.save(foundTraining);
        if (trainingDto.isSendInvite()) {
            List<String> failToSend = notificationService.sendInvitationForParticipants(trainingWithParticipants);
            if (!failToSend.isEmpty())
                throw new FailToSendEmail(failToSend, TrainingMapper.mapToTrainingDto(savedTraining));
        }
        log.info("Training saved to the database: \n" + savedTraining);

        return TrainingMapper.mapToTrainingDto(savedTraining);
    }

    public void updateById(Long id, TrainingDto trainingDto, Optional<MultipartFile> icsFile) throws InvalidFieldsException, InvalidFileException {
        Training currentTraining = trainingRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Training", id));

        TrainingValidator.validateDates(trainingDto);
        TrainingValidator.validateIcsFile(icsFile);

        Training updatedTraining = mapToTraining(trainingDto, icsFile);
        updatedTraining.setId(id);
        updatedTraining.setArchived(currentTraining.isArchived());
        updatedTraining.setCreatedAt(currentTraining.getCreatedAt());
        updatedTraining.setEditedAt(Timestamp.from(Instant.now()));
        Training savedTraining = trainingRepository.save(updatedTraining);

        log.info("Training details updated");

        updateParticipantsList(savedTraining, trainingDto);

        Training trainingWithParticipants = trainingRepository.save(savedTraining);
        if (trainingDto.isSendInvite()) {
            List<String> failToSend = notificationService.sendInvitationForParticipants(trainingWithParticipants, true);
            if (!failToSend.isEmpty())
                throw new FailToSendEmail(failToSend);
        }

        log.info("Training participants updated");
    }

    public TrainingDto getById(long id) {
        Training training = trainingRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Training", id));
        return TrainingMapper.mapToTrainingDto(training);
    }

    public List<TrainingDto> getAll() {
        return trainingRepository.findAllByOrderByEditedAtDesc().stream()
                .map(TrainingMapper::mapToTrainingDto)
                .collect(Collectors.toList());
    }

    private void addNewSkills(Set<SkillDto> skillDtos) {
        Set<SkillDto> copyOfSkillDtos = new HashSet<>(skillDtos);
        Set<Skill> skillsFound = skillRepository.findByNameIn(copyOfSkillDtos.stream()
                .map(SkillDto::getName)
                .collect(Collectors.toSet()));

        copyOfSkillDtos.removeAll(skillsFound.stream()
                .map(SkillMapper::mapToSkillDto)
                .collect(Collectors.toSet()));

        Set<Skill> skillsToBeAdded = copyOfSkillDtos.stream()
                .map(this::mapToSkill)
                .collect(Collectors.toSet());

        skillRepository.saveAll(skillsToBeAdded);
    }

    public boolean participantExists(UUID invitationCode) {
        return trainingParticipantRepository.findByInvitationCode(invitationCode).isPresent();
    }

    public void deleteByIdTraining(Long id) {
        Training training = trainingRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Training", id));
        training.setArchived(true);
        trainingRepository.save(training);
    }

    public List<Training> filterTrainings(Filter filter) {
        return trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(), filter.getLocations(), filter.getDiscipline(), filter.getTrainers(), filter.getEndDate(), filter.getStartDate());
    }

    public List<TrainingDto> getByPage(Long pageNumber) {
        if (pageNumber < 1) {
            throw new InvalidPageException(MessageConstants.negativePageNumberMessage);
        }
        List<TrainingDto> trainingDtos = getByPage(trainingRepository.findAllByOrderByEditedAtDesc(), pageNumber, PAGE_SIZE);
        if (trainingDtos.size() == 0) {
            throw new InvalidPageException(String.format(MessageConstants.pageDoesNotExistsMessage, pageNumber));
        }
        return trainingDtos;
    }

    public FilterTrainingResultDto filterTrainingsAndPaginateThem(Filter filter, Long pageNumber) {
        filter.toLowerCase();
        if (pageNumber <= 0) {
            throw new InvalidPageException(MessageConstants.negativePageNumberMessage);
        }
        PageRequest page = PageRequest.of(Math.toIntExact(pageNumber) - 1, 10, Sort.by("editedAt").descending());

        Page<Training> paginatedTrainings = trainingRepository.filterTrainings(page, filter.getNames(), filter.getSkills(), filter.getLocations(), filter.getDiscipline(),
                filter.getTrainers(), filter.getEndDate(), filter.getStartDate());

        if (paginatedTrainings.getTotalPages() < pageNumber && paginatedTrainings.getTotalElements() != 0) {
            throw new InvalidPageException(String.format("The page with the number %d does not exist!", pageNumber));
        }
        List<TrainingDto> paginatedTrainingsDto = paginatedTrainings.getContent().stream().map(TrainingMapper::mapToTrainingDto).collect(Collectors.toList());

        return FilterTrainingResultDto.builder()
                .totalResults(paginatedTrainings.getTotalElements())
                .results(paginatedTrainingsDto)
                .build();
    }

    public Long getNumberOfTrainings() {
        return trainingRepository.getNumberOfTrainings();
    }

    public List<TrainingDto> getByPage(List<Training> trainingsList, Long pageNumber, int pageSize) {
        List<TrainingDto> trainingDtos = trainingsList.stream()
                .map(TrainingMapper::mapToTrainingDto)
                .skip((pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
        return trainingDtos;
    }

    private Set<TrainingParticipant> getParticipantList(TrainingDto dto, Training training) {
        Set<String> emails = dto.getParticipants()
                .stream()
                .map(e -> e.toLowerCase().trim())
                .collect(Collectors.toSet());
        Set<Employee> employees = employeeRepository.findByEmailIn(emails);

        return employees.stream().map(emp -> createTrainingParticipant(emp, training)).collect(Collectors.toSet());
    }

    private Set<String> getEmployeeEmailsByTrainingId(Long trainingId) {
        return trainingParticipantRepository.findAllByTrainingId(trainingId).stream()
                .map(TrainingParticipant::getEmployee)
                .map(Employee::getEmail)
                .collect(Collectors.toSet());
    }

    private Set<TrainingParticipant> addNewParticipantsToTraining(Set<String> emails, Training training) {
        emails.removeAll(getEmployeeEmailsByTrainingId(training.getId()));
        return employeeRepository.findByEmailIn(emails).stream()
                .map(employee -> createTrainingParticipant(employee, training))
                .collect(Collectors.toSet());
    }

    private void removeEliminatedParticipants(Set<String> addedParticipants, Long trainingId) {
        Set<String> allEmails = getEmployeeEmailsByTrainingId(trainingId);
        allEmails.removeAll(addedParticipants);
        addedParticipants.removeAll(allEmails);
        trainingParticipantRepository.deleteAllEmployeesByEmailIn(allEmails);
    }

    private void updateParticipantsList(Training training, TrainingDto dto) {
        Set<String> emails = dto.getParticipants()
                .stream()
                .map(email -> email.toLowerCase().trim())
                .collect(Collectors.toSet());

        removeEliminatedParticipants(emails, training.getId());
        training.getParticipants().addAll(addNewParticipantsToTraining(emails, training));
        trainingRepository.save(training);
    }

    private TrainingParticipant createTrainingParticipant(Employee employee, Training training) {
        TrainingParticipant trainingParticipant = new TrainingParticipant();
        trainingParticipant.setEmployee(employee);
        trainingParticipant.setStatus(getStatusByTrainingStartDate(Optional.ofNullable(training.getStartDate())));
        trainingParticipant.setTraining(training);
        return trainingParticipant;
    }

    private Training mapToTraining(TrainingDto trainingDto, Optional<MultipartFile> icsFile) throws InvalidFileException {
        Training training = new Training();

        formatStringFields(trainingDto);

        training.setLocation(trainingDto.getLocation());
        training.setName(trainingDto.getName());
        training.setIsInternal(trainingDto.getIsInternal());
        training.setDescription(trainingDto.getDescription());
        training.setStartDate(trainingDto.getStartDate());
        training.setEndDate(trainingDto.getEndDate());
        training.setMeetingURL(trainingDto.getMeetingURL());

        addNewSkills(trainingDto.getSkills());
        training.setSkills(skillRepository.findByNameIn(trainingDto.getSkills().stream()
                .map(SkillDto::getName)
                .collect(Collectors.toSet())));
        training.setDiscipline(trainingDto.getDiscipline() != null ? findDisciplineByName(trainingDto.getDiscipline()) : null);
        training.setTrainers(trainingDto.getTrainers().stream()
                .map(this::findEmployeeByName)
                .collect(Collectors.toSet()));

        if (icsFile.isPresent()) {
            try {
                training.setIcs(icsFile.get().getBytes());
            } catch (IOException e) {
                log.error("Error trying to get bytes from the ICS file.");
                throw new InvalidFileException("There was an error trying to save this file.");
            }
            training.setIcsFilename(icsFile.get().getOriginalFilename());
        }
        return training;
    }

    private Employee findEmployeeByName(EmployeeDto employeeDto) {
        String errorMessage = String.format("Employee %s %s not found", employeeDto.getFirstName(), employeeDto.getLastName());
        return employeeRepository.findByFirstNameAndLastName(employeeDto.getFirstName(), employeeDto.getLastName())
                .orElseThrow(() -> new EntityNotFoundException(errorMessage));
    }

    private Discipline findDisciplineByName(DisciplineDto disciplineDto) {
        String errorMessage = String.format("Discipline %s not found.", disciplineDto.getName());
        return disciplineRepository.findByNameIgnoreCase(removeExtraWhitespaces(disciplineDto.getName())).orElseThrow(() -> new EntityNotFoundException(errorMessage));
    }

    private Skill mapToSkill(SkillDto skillDto) {
        Skill skill = new Skill();
        skill.setName(skillDto.getName());
        return skill;
    }

    private void formatStringFields(TrainingDto trainingDto) {
        if (trainingDto.getSkills() != null) {
            trainingDto.getSkills().forEach(skillDto -> skillDto.setName(capitalize(skillDto.getName())));
        }

        if (trainingDto.getDiscipline() != null) {
            trainingDto.getDiscipline().setName(trainingDto.getDiscipline().getName());
        }

        trainingDto.getTrainers().forEach(trainer -> {
            trainer.setFirstName(capitalize(trainer.getFirstName()));
            trainer.setLastName(capitalize(trainer.getLastName()));
        });
    }

    private StatusInvitation getStatusByTrainingStartDate(Optional<Timestamp> trainingStartDate) {
        if (trainingStartDate.isPresent()) {
            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
            return currentTimestamp.after(trainingStartDate.get()) ? StatusInvitation.Participated : StatusInvitation.Invited;
        }
        return StatusInvitation.Invited;
    }
}
