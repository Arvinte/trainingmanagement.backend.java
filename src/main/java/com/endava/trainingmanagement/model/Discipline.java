package com.endava.trainingmanagement.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "disciplines", schema = "trainings_schema")
public class Discipline {
    @Id
    @SequenceGenerator(name = "discipline_id_gen", sequenceName = "trainings_schema.seq_discipline", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "discipline_id_gen")
    @Column(name = "id")
    private Long id;

    @OneToMany(mappedBy = "discipline")
    private Set<Training> trainings = new HashSet<>();

    @OneToMany(mappedBy = "discipline")
    private Set<Employee> employees = new HashSet<>();

    @Column(name = "name")
    private String name;
}
