package com.endava.trainingmanagement.model;

import com.endava.trainingmanagement.constants.StatusInvitation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "trainings_participants", schema = "trainings_schema")
public class TrainingParticipant implements Serializable {
    @Id
    @SequenceGenerator(name = "training_participant_id_gen", sequenceName = "trainings_schema.seq_training_participant", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "training_participant_id_gen")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "training_id")
    private Training training;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private StatusInvitation status;

    @Column(name = "invitation_code")
    private UUID invitationCode;

    @Column(name = "modification_date")
    private Timestamp modificationDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrainingParticipant)) return false;
        TrainingParticipant that = (TrainingParticipant) o;
        return getTraining().equals(that.getTraining()) &&
                getEmployee().equals(that.getEmployee()) &&
                getStatus().equals(that.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTraining(), getEmployee(), getStatus());
    }
}
