package com.endava.trainingmanagement.model;

import com.endava.trainingmanagement.constraint.NameConstraint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "employees", schema = "trainings_schema")
public class Employee {
    @Id
    @SequenceGenerator(name = "employee_id_gen", sequenceName = "trainings_schema.seq_employee", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_id_gen")
    @Column(name = "id")
    private Long id;

    @ManyToMany
    @JoinTable(name = "employees_roles", schema = "trainings_schema",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "trainers")
    private Set<Training> trainings = new HashSet<>();

    @OneToMany(mappedBy = "employee")
    private Set<TrainingParticipant> trainingParticipants = new HashSet<>();

    @ManyToOne()
    @JoinColumn(name = "discipline_id")
    private Discipline discipline;

    @NameConstraint
    @Column(name = "first_name")
    private String firstName;

    @NameConstraint
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;
}
