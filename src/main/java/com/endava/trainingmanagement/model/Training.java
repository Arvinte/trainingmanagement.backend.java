package com.endava.trainingmanagement.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Where(clause = "is_archived = false")
@Entity
@Table(name = "trainings", schema = "trainings_schema")
public class Training {
    @Id
    @SequenceGenerator(name = "training_id_gen", sequenceName = "trainings_schema.seq_training", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "training_id_gen")
    @Column(name = "id")
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinTable(name = "trainings_trainers", schema = "trainings_schema",
            joinColumns = @JoinColumn(name = "training_id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id"))
    private Set<Employee> trainers = new HashSet<>();

    @OneToMany(mappedBy = "training", cascade = {CascadeType.ALL, CascadeType.REFRESH})
    private Set<TrainingParticipant> participants = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinTable(name = "trainings_skills", schema = "trainings_schema",
            joinColumns = @JoinColumn(name = "training_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private Set<Skill> skills = new HashSet<>();

    @ManyToOne()
    @JoinColumn(name = "discipline_id")
    private Discipline discipline;

    @Column(name = "name")
    private String name;

    @Column(name = "location")
    private String location;

    @Column(name = "description")
    private String description;

    @Column(name = "start_date")
    private Timestamp startDate;

    @Column(name = "end_date")
    private Timestamp endDate;

    @Column(name = "is_archived")
    private boolean isArchived;

    @Column(name = "is_internal")
    private Boolean isInternal;

    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "edited_at")
    private Timestamp editedAt;

    @Column(name = "meeting_url")
    private String meetingURL;

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(name = "ics")
    private byte[] ics;

    @Column(name = "ics_filename")
    private String icsFilename;
}
