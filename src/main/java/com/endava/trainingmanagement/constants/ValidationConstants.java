package com.endava.trainingmanagement.constants;

import java.util.ArrayList;
import java.util.Arrays;

public final class ValidationConstants {
    public static final int TRAINING_NAME_MAX_LENGTH = 200;
    public static final int NAME_MAX_LENGTH = 50;
    public static final int SKILL_NAME_MAX_LENGTH = 200;
    public static final ArrayList<String> locations = new ArrayList<>(Arrays.asList("On-site", "Online", "Off-site"));
}

