package com.endava.trainingmanagement.constants;

public enum StatusInvitation {
    Invited("Invited"),
    Fail("Fail to send"),
    Confirmed("Confirmed"),
    Declined("Declined"),
    Participated("Participated");

    private final String value;

    StatusInvitation(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}