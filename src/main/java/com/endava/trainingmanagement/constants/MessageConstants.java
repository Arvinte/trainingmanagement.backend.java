package com.endava.trainingmanagement.constants;

public class MessageConstants {
    public static final String negativePageNumberMessage = "Negative page numbers are not allowed!";
    public static final String pageDoesNotExistsMessage = "The page with the number %d does not exist!";
}
