package com.endava.trainingmanagement.util;

import com.endava.trainingmanagement.exceptions.InvalidStringException;

import java.util.Arrays;
import java.util.StringJoiner;

public final class StringUtils {
    public static String capitalize(String string) {
        if (string.trim().equals("")) {
            throw new InvalidStringException("String should not be empty/not contain only whitespaces.");
        }

        string = string.toLowerCase();
        string = string.trim();

        StringJoiner stringJoiner = new StringJoiner(" ");
        Arrays.stream(string.split("\\s+"))
                .map(word -> word.substring(0, 1).toUpperCase() + word.substring(1))
                .forEach(stringJoiner::add);

        return stringJoiner.toString();
    }

    public static String removeExtraWhitespaces(String string) {
        return string.trim().replaceAll("\\s+", " ");
    }
}
