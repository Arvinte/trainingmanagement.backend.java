package com.endava.trainingmanagement.utils;

import com.endava.trainingmanagement.email.ConfirmationMessage;
import com.endava.trainingmanagement.email.InvitationMessage;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.time.format.DateTimeFormatter;


@Component
public final class EmailMapper {
    private final SpringTemplateEngine templateEngine;
    private final DateTimeFormatter formatter;

    public EmailMapper(SpringTemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
        formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");
    }

    public String getBodyEmail(InvitationMessage notification) {
        final Context ctx = new Context();
        ctx.setVariable("name", notification.getName());
        ctx.setVariable("trainingName", notification.getTrainingName());
        if (notification.getDate() != null) {
            ctx.setVariable("date", notification.getDate().format(formatter));
        } else {
            ctx.setVariable("date", "TBA");
        }
        ctx.setVariable("confirmUrl", notification.getConfirmationUrl());
        return this.templateEngine.process("invitationEmail.html", ctx);
    }

    public String getBodyEmail(ConfirmationMessage notification) {
        final Context ctx = new Context();
        ctx.setVariable("nameParticipant", notification.getNameParticipant());
        ctx.setVariable("nameTraining", notification.getTrainingName());
        if (notification.getDateOfTraining() != null) {
            ctx.setVariable("date", notification.getDateOfTraining());
        } else {
            ctx.setVariable("date", "TBA");
        }
        ctx.setVariable("url", notification.getMeetingUrl());
        ctx.setVariable("trainers", notification.getTrainer());
        return this.templateEngine.process("confirmation.html", ctx);
    }
}
