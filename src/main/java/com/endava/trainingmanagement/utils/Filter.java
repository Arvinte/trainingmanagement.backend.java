package com.endava.trainingmanagement.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Filter {
    private List<String> trainers;
    private List<String> locations;
    private List<String> names;
    private LocalDate startDate=LocalDate.of(1970,10,12);
    private LocalDate endDate=LocalDate.of(2100,10,12);
    private List<String> skills;
    private String discipline;

    public void toLowerCase(){
        if(this.trainers!=null){
            this.trainers = this.trainers.stream().map(trainerName->trainerName.toLowerCase()).collect(Collectors.toList());
        }
        if(this.names!=null){
            this.names = this.names.stream().map(trainingName->trainingName.toLowerCase()).collect(Collectors.toList());
        }
        if(this.skills!=null){
            this.skills = this.skills.stream().map(skillName->skillName.toLowerCase()).collect(Collectors.toList());
        }
    }
}
