package com.endava.trainingmanagement.utils;

import com.endava.trainingmanagement.email.InvitationMessage;
import com.endava.trainingmanagement.model.Training;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EmailUtils {

    public static InvitationMessage getTemplateInvitationEmail(Training training, boolean update) {
        log.debug("Creating template invitation email");
        InvitationMessage invitationEmail = new InvitationMessage();
        invitationEmail.setTrainingName(training.getName());
        if (training.getStartDate() != null) {
            invitationEmail.setDate(training.getStartDate().toLocalDateTime().toLocalDate());
        }
        if (update) {
            invitationEmail.setSubjectToModified();
        }
        return invitationEmail;
    }


    public static String createUrlConfirmation(UUID uuid, String baseUrl) {
        return String.format("%s/confirmation/%s", baseUrl, uuid);
    }

}
