package com.endava.trainingmanagement.constraint;

import com.endava.trainingmanagement.validator.DisciplineNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = DisciplineNameValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DisciplineNameConstraint {
    String message() default "Invalid discipline";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}