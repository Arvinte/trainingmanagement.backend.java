package com.endava.trainingmanagement.constraint;

import com.endava.trainingmanagement.validator.LocationValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = LocationValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LocationConstraint {
    String message() default "Invalid location";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}