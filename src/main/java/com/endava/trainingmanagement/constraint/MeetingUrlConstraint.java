package com.endava.trainingmanagement.constraint;

import com.endava.trainingmanagement.validator.MeetingUrlValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = MeetingUrlValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MeetingUrlConstraint {
    String message() default "Invalid meeting URL";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}