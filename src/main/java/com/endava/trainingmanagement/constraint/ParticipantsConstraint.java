package com.endava.trainingmanagement.constraint;

import com.endava.trainingmanagement.validator.ParticipantsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = ParticipantsValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ParticipantsConstraint {
    String message() default "There is an invalid participant.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}