package com.endava.trainingmanagement.email;

import com.endava.trainingmanagement.utils.EmailMapper;
import lombok.Data;

import java.time.LocalDate;

@Data
public class InvitationMessage implements EmailMessage {

    private String name;
    private String trainingName;
    private LocalDate date;
    private String confirmationUrl;
    private boolean update;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAsHtml(EmailMapper mapper) {
        return mapper.getBodyEmail(this);
    }

    @Override
    public String getSubject() {
        return update ? "Training confirmation required - Modified" : "Invitation training";

    }

    public void setSubjectToModified() {
        update = true;
    }

    public InvitationMessage clone() {
        InvitationMessage invitationMessage = new InvitationMessage();
        invitationMessage.setConfirmationUrl(confirmationUrl);
        invitationMessage.setName(name);
        invitationMessage.setTrainingName(trainingName);
        invitationMessage.setDate(date);
        return invitationMessage;
    }
}
