package com.endava.trainingmanagement.email;

import com.endava.trainingmanagement.utils.EmailMapper;

public interface EmailMessage {
    String getAsHtml(EmailMapper mapper);

    String getSubject();
}
