package com.endava.trainingmanagement.email;

import com.endava.trainingmanagement.utils.EmailMapper;
import lombok.Data;

@Data
public class ConfirmationMessage implements EmailMessage {
    private String nameParticipant;
    private String dateOfTraining;
    private String trainer;
    private String meetingUrl;
    private String trainingName;

    @Override
    public String getAsHtml(EmailMapper mapper) {
        return mapper.getBodyEmail(this);
    }

    @Override
    public String getSubject() {
        return "Confirmed training details";
    }
}
