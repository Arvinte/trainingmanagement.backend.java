package com.endava.trainingmanagement.mapper;

import com.endava.trainingmanagement.dto.TrainingDto;
import com.endava.trainingmanagement.model.Training;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Slf4j
@Component
public final class TrainingMapper {
    private TrainingMapper() {
    }

    public static TrainingDto mapToTrainingDto(Training training) {
        TrainingDto trainingDto = new TrainingDto();

        trainingDto.setId(training.getId());
        trainingDto.setTrainers(training.getTrainers().stream().map(EmployeeMapper::mapToEmployeeDto).collect(Collectors.toSet()));
        trainingDto.setParticipants(training.getParticipants().stream().map(participant -> participant.getEmployee().getEmail()).collect(Collectors.toSet()));
        trainingDto.setSkills(training.getSkills().stream().map(SkillMapper::mapToSkillDto).collect(Collectors.toSet()));
        trainingDto.setDiscipline(DisciplineMapper.mapToDisciplineDto(training.getDiscipline()));
        trainingDto.setIsInternal(training.getIsInternal());
        trainingDto.setName(training.getName());
        trainingDto.setLocation(training.getLocation());
        trainingDto.setDescription(training.getDescription());
        trainingDto.setStartDate(training.getStartDate());
        trainingDto.setEndDate(training.getEndDate());
        trainingDto.setMeetingURL(training.getMeetingURL());
        trainingDto.setIsInternal(training.getIsInternal());
        trainingDto.setIcsFilename(training.getIcsFilename());

        return trainingDto;
    }
}