package com.endava.trainingmanagement.mapper;

import com.endava.trainingmanagement.dto.EmployeeDto;
import com.endava.trainingmanagement.model.Employee;

public final class EmployeeMapper {
    private EmployeeMapper() {
    }

    public static EmployeeDto mapToEmployeeDto(Employee employee) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setEmail(employee.getEmail());
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());
        employeeDto.setDiscipline(employee.getDiscipline() != null ? DisciplineMapper.mapToDisciplineDto(employee.getDiscipline()) : null);
        return employeeDto;
    }


}
