package com.endava.trainingmanagement.mapper;

import com.endava.trainingmanagement.dto.DisciplineDto;
import com.endava.trainingmanagement.model.Discipline;

public final class DisciplineMapper {
    private DisciplineMapper() {
    }

    public static DisciplineDto mapToDisciplineDto(Discipline discipline) {
        if (discipline == null) {
            return null;
        }
        DisciplineDto disciplineDto = new DisciplineDto();
        disciplineDto.setName(discipline.getName());
        return disciplineDto;
    }
}
