package com.endava.trainingmanagement.mapper;

import com.endava.trainingmanagement.dto.RoleDto;
import com.endava.trainingmanagement.model.Role;

public final class RoleMapper {
    private RoleMapper() {
    }

    public static RoleDto mapToRoleDto(Role role) {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(role.getId());
        roleDto.setName(role.getName());
        return roleDto;
    }

}
