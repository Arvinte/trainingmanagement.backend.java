package com.endava.trainingmanagement.mapper;

import com.endava.trainingmanagement.dto.SkillDto;
import com.endava.trainingmanagement.model.Skill;

public final class SkillMapper {
    private SkillMapper() {
    }

    public static SkillDto mapToSkillDto(Skill skill) {
        SkillDto skillDto = new SkillDto();
        skillDto.setName(skill.getName());
        skillDto.setId(skill.getId());
        return skillDto;
    }
}
