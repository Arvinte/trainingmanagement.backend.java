package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.service.StatusParticipationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/confirmation")
@AllArgsConstructor
public class StatusParticipationController {

    private final StatusParticipationService service;

    @GetMapping("/{code}")
    public void updateStatusParticipant(@PathVariable final UUID code, HttpServletResponse response) throws IOException {
         service.updateStatus(code,response);

    }
}
