package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.dto.FilterTrainingReportResultDTO;
import com.endava.trainingmanagement.service.FilterReportsService;
import com.endava.trainingmanagement.service.TrainingCsvService;
import com.endava.trainingmanagement.utils.Filter;
import com.endava.trainingmanagement.utils.ReportFilter;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(path = "reports")
public class ReportController {
    private final FilterReportsService reportService;
    private final TrainingCsvService trainingCsvService;

    @PostMapping("/filter/{page}")
    @ResponseStatus(HttpStatus.OK)
    public List<FilterTrainingReportResultDTO> getTrainingsForReport(@RequestBody ReportFilter reportFilter,
                                                                     @PathVariable @Valid @Min(1) final Long page) {
        return this.reportService.getTrainingsForReportPaginated(page, reportFilter);
    }

    @PostMapping("/filter/size")
    @ResponseStatus(HttpStatus.OK)
    public int getSizeOfReport(@RequestBody ReportFilter reportFilter) {
        return reportService.getReportSize(reportFilter);
    }

    @GetMapping("/employees")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ByteArrayResource> getEmployeesReport(ReportFilter reportFilter) {
        return trainingCsvService.getEmployeesReportFromFilters(reportFilter);
    }

    @GetMapping("/trainings")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ByteArrayResource> getTrainingsReport(Filter filter) {
        return trainingCsvService.getTrainingsReportFromFilters(filter);
    }
}
