package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.dto.EmployeeDto;
import com.endava.trainingmanagement.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginController {
    private final EmployeeService employeeService;

    @PostMapping(value = "/login")
    public EmployeeDto login(@RequestBody String email) {
        return employeeService.getByEmail(email);
    }
}
