package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.dto.DisciplineDto;
import com.endava.trainingmanagement.service.DisciplineService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(path = "disciplines")
public class DisciplineController {
    private final DisciplineService disciplineService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<DisciplineDto> getAll() {
        return disciplineService.getAll();
    }
}
