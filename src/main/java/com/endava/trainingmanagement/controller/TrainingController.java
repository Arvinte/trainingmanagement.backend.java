package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.dto.FilterTrainingResultDto;
import com.endava.trainingmanagement.dto.TrainingDto;
import com.endava.trainingmanagement.exceptions.InvalidFieldsException;
import com.endava.trainingmanagement.exceptions.InvalidFileException;
import com.endava.trainingmanagement.service.TrainingCsvService;
import com.endava.trainingmanagement.service.TrainingService;
import com.endava.trainingmanagement.utils.Filter;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import java.util.Optional;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(path = "trainings")
public class TrainingController {
    private final TrainingService trainingService;
    private final TrainingCsvService trainingCsvService;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public TrainingDto createTraining(@RequestPart(name = "training") @Valid TrainingDto trainingDto,
                                      @RequestPart(name = "icsFile") Optional<MultipartFile> icsFile) throws InvalidFieldsException, InvalidFileException {
        return trainingService.saveTraining(trainingDto, icsFile);
    }

    @PutMapping(path = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void update(@PathVariable @Valid @Min(1) Long id,
                       @RequestPart(name = "training") @Valid TrainingDto trainingDto,
                       @RequestPart(name = "icsFile") Optional<MultipartFile> icsFile) throws InvalidFieldsException, InvalidFileException {
        trainingService.updateById(id, trainingDto, icsFile);
    }

    @GetMapping("/{id}")
    public TrainingDto getTrainingById(@PathVariable final long id) {
        return trainingService.getById(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<TrainingDto> getAll() {
        return trainingService.getAll();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTrainingById(@PathVariable @Valid @Min(1) final Long id) {
        trainingService.deleteByIdTraining(id);
    }

    @GetMapping("/page/{page}")
    @ResponseStatus(HttpStatus.OK)
    public List<TrainingDto> getTrainingsOnPage(@PathVariable @Valid @Min(1) final Long page) {
        return trainingService.getByPage(page);
    }

    @GetMapping("/numberOfTrainings")
    @ResponseStatus(HttpStatus.OK)
    public Long getNumberOfTrainings() {
        return trainingService.getNumberOfTrainings();
    }

    @GetMapping("/{id}/participants")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ByteArrayResource> getTrainingParticipantsCsv(@PathVariable final long id) {
        return trainingCsvService.getTrainingParticipantsCsv(id);
    }

    @PostMapping("/filter/{page}")
    @ResponseStatus(HttpStatus.OK)
    public FilterTrainingResultDto filterTrainings(@RequestBody Filter filter,
                                                   @PathVariable @Valid @Min(1) final Long page) {
        return trainingService.filterTrainingsAndPaginateThem(filter, page);
    }
}
