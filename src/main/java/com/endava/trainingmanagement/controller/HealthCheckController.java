package com.endava.trainingmanagement.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("health")
public class HealthCheckController {
    @GetMapping
    public String getHealth() {
        return "This application doesn't have covid-19";
    }
}

