package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(path = "employees")
public class EmployeeController {
    private final EmployeeService employeeService;

    @GetMapping("/exists")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> getExistsEmployees(@RequestParam("list") List<String> list) {
        return employeeService.existsEmployees(list);
    }
}
