package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.service.TrainingService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping(path = "validation")
public class ParticipationValidationController {
    private final TrainingService trainingService;

    @GetMapping(path = "/{invitationCode}")
    @ResponseStatus(HttpStatus.OK)
    public boolean isValid(@PathVariable @Valid UUID invitationCode) {
        return trainingService.participantExists(invitationCode);
    }
}
