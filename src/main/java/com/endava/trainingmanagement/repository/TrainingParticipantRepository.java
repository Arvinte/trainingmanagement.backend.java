package com.endava.trainingmanagement.repository;

import com.endava.trainingmanagement.model.TrainingParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface TrainingParticipantRepository extends JpaRepository<TrainingParticipant, Long> {
    Set<TrainingParticipant> findAllByTrainingId(Long id);

    Optional<TrainingParticipant> findByInvitationCode(UUID codeInvitation);

    @Transactional
    @Modifying
    @Query("delete from TrainingParticipant tp where tp.employee.id in (select e.id from Employee e where e.email in (:emails))")
    void deleteAllEmployeesByEmailIn(@Param("emails") Set<String> emails);

}
