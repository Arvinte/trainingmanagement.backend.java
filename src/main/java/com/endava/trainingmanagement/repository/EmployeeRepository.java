package com.endava.trainingmanagement.repository;

import com.endava.trainingmanagement.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> getEmployeeByEmail(String email);

    Set<Employee> findByEmailIn(Set<String> emails);

    Optional<Employee> findByFirstNameAndLastName(String firstName, String lastName);

    @Query("select e from Employee e where concat(concat(e.firstName,' '), e.lastName) = :name")
    Optional<Employee> findByFirstNameAndLastName(String name);
}
