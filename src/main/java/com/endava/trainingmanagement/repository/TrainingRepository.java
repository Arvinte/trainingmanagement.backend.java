package com.endava.trainingmanagement.repository;


import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.utils.ReportFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TrainingRepository extends JpaRepository<Training, Long> {
    List<Training> findAllByOrderByEditedAtDesc();

    @Query("select count(t) from Training t")
    Long getNumberOfTrainings();

    @Query("select distinct t from Training t  left join t.skills s left  join t.trainers tr left  join t.discipline d  where " +
            "(lower(t.name) in :names or :names is null) and (lower(s.name) in :skills or :skills is null or s.name is null) and" +
            " (t.location in :location or :location is null or t.location is null ) and " +
            " (d.name = :discipline or :discipline is null or d.name is null) and " +
            "(lower(tr.firstName) in :trainers or lower(tr.lastName) in :trainers or :trainers is null) and " +
            "((cast(:startDate as date) <= cast(t.startDate as date) or t.startDate is null) and (cast(:endDate as date)>= cast(t.endDate as date) or t.endDate is null))")
    public Page<Training> filterTrainings(Pageable pageable, List<String> names, List<String> skills, List<String> location, String discipline, List<String> trainers, LocalDate endDate, LocalDate startDate);

    @Query("select distinct t from Training t  left join t.skills s left  join t.trainers tr left  join t.discipline d  where " +
            "(lower(t.name) in :names or :names is null) and (lower(s.name) in :skills or :skills is null or s.name is null) and" +
            " (t.location in :location or :location is null or t.location is null) and " +
            " (d.name = :discipline or :discipline is null or d.name is null) and " +
            "(lower(tr.firstName) in :trainers or lower(tr.lastName) in :trainers or :trainers is null) and " +
            "((cast(:startDate as date) <= cast(t.startDate as date) or t.startDate is null) and (cast(:endDate as date)>= cast(t.endDate as date) or t.endDate is null))")
    public List<Training> filterTrainings(List<String> names, List<String> skills, List<String> location, String discipline, List<String> trainers, LocalDate endDate, LocalDate startDate);

    @Query("select  distinct t" +
            " from Training t left join t.trainers tr" +
            " left join t.skills s" +
            " left join t.participants p" +
            " inner join t.discipline d " +
            " join p.employee e" +
            " where " +
            "(lower(t.name) in :#{#reportFilter.names} or :#{#reportFilter.names} is null) and " +
            "(lower(tr.lastName) in :#{#reportFilter.trainers} or lower(tr.firstName) in :#{#reportFilter.trainers} or :#{#reportFilter.trainers} is null) and" +
            "(p.employee.firstName in :#{#reportFilter.traineeName} or p.employee.lastName in :#{#reportFilter.traineeName} or :#{#reportFilter.traineeName} is null) and" +
            "(lower(s.name) in :#{#reportFilter.skills} or :#{#reportFilter.skills} is null) and " +
            "((cast(:#{#reportFilter.startDate} as date) <= cast(t.startDate as date) and cast(:#{#reportFilter.endDate} as date)>= cast(t.endDate as date)))")
    public List<Training> getTrainingsForReport(ReportFilter reportFilter);
}


