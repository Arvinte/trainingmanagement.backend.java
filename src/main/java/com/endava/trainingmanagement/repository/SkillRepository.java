package com.endava.trainingmanagement.repository;

import com.endava.trainingmanagement.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {
    Set<Skill> findByNameIn(Set<String> names);

    Optional<Skill> findByName(String name);
}
