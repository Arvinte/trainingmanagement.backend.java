package com.endava.trainingmanagement.validator;

import com.endava.trainingmanagement.constraint.MeetingUrlConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MeetingUrlValidator implements ConstraintValidator<MeetingUrlConstraint, String> {
    @Override
    public void initialize(MeetingUrlConstraint url) {
    }

    @Override
    public boolean isValid(String url, ConstraintValidatorContext cxt) {
        if (url == null) return true;
        return url.matches("^https(s)?://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
    }
}