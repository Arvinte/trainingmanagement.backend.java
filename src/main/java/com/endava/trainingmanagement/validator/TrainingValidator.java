package com.endava.trainingmanagement.validator;

import com.endava.trainingmanagement.dto.TrainingDto;
import com.endava.trainingmanagement.exceptions.InvalidFieldsException;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;
import java.util.Optional;

public final class TrainingValidator {
    private TrainingValidator() {
    }

    public static void validateDates(TrainingDto trainingDto) throws InvalidFieldsException {
        if (trainingDto.getStartDate() == null && trainingDto.getEndDate() == null)
            return;
        if (trainingDto.getStartDate() == null && trainingDto.getEndDate() != null)
            throw new InvalidFieldsException("Can't have end date without start date.");

        if (trainingDto.getEndDate()!=null && trainingDto.getStartDate().compareTo(trainingDto.getEndDate()) > 0) {
            throw new InvalidFieldsException("'End date' should be after 'Start date'.");
        }
    }

    public static void validateIcsFile(Optional<MultipartFile> icsFile) throws InvalidFieldsException {
        if (icsFile.isPresent()) {
            if (!Objects.requireNonNull(icsFile.get().getOriginalFilename()).endsWith(".ics")) {
                throw new InvalidFieldsException("The invitation file is not an '.ics' file.");
            }
        }
    }
}
