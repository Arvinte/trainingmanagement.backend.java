package com.endava.trainingmanagement.validator;

import com.endava.trainingmanagement.constants.ValidationConstants;
import com.endava.trainingmanagement.constraint.LocationConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LocationValidator implements ConstraintValidator<LocationConstraint, String> {
    @Override
    public void initialize(LocationConstraint location) {
    }

    @Override
    public boolean isValid(String location, ConstraintValidatorContext cxt) {
        if (location == null)
            return true;
        return ValidationConstants.locations.contains(location);
    }
}
