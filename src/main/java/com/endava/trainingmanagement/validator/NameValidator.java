package com.endava.trainingmanagement.validator;

import com.endava.trainingmanagement.constants.ValidationConstants;
import com.endava.trainingmanagement.constraint.NameConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidator implements ConstraintValidator<NameConstraint, String> {
    @Override
    public void initialize(NameConstraint name) {
    }

    @Override
    public boolean isValid(String name, ConstraintValidatorContext cxt) {
        return name.matches("^[a-zA-Z\\s]+") && (name.length() <= ValidationConstants.NAME_MAX_LENGTH);
    }
}