package com.endava.trainingmanagement.validator;

import com.endava.trainingmanagement.constraint.ParticipantsConstraint;
import org.apache.commons.validator.EmailValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

public class ParticipantsValidator implements ConstraintValidator<ParticipantsConstraint, Set<String>> {
    @Override
    public boolean isValid(Set<String> participants, ConstraintValidatorContext cxt) {
        EmailValidator emailValidator = EmailValidator.getInstance();
        return participants.stream().allMatch(emailValidator::isValid);
    }
}
