package com.endava.trainingmanagement.validator;

import com.endava.trainingmanagement.constraint.DisciplineNameConstraint;
import com.endava.trainingmanagement.model.Discipline;
import com.endava.trainingmanagement.repository.DisciplineRepository;
import static com.endava.trainingmanagement.util.StringUtils.removeExtraWhitespaces;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class DisciplineNameValidator implements ConstraintValidator<DisciplineNameConstraint, String> {
    @Autowired
    private DisciplineRepository disciplineRepository;

    @Override
    public void initialize(DisciplineNameConstraint disciplineName) {
    }

    @Override
    public boolean isValid(String disciplineName, ConstraintValidatorContext cxt) {
        Optional<Discipline> discipline = disciplineRepository.findByNameIgnoreCase(removeExtraWhitespaces(disciplineName));

        return discipline.isPresent();
    }
}