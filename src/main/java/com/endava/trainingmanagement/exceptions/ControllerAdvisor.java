package com.endava.trainingmanagement.exceptions;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException exception, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());
        log.error("handleConflict",exception);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, Object> response = new LinkedHashMap<>();
        Map<String, Object> body = new LinkedHashMap<>();
        response.put("timestamp", LocalDateTime.now());
        for (FieldError fieldError : exception.getBindingResult().getFieldErrors())
            body.put(fieldError.getField(), fieldError.getDefaultMessage());
        response.put("messages", body);
        return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {TransactionSystemException.class})
    protected ResponseEntity<Object> handleConstraintException(TransactionSystemException exception, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        log.error("handleConstraintException",exception);

        if (exception.getCause() instanceof RollbackException) {
            RollbackException rollbackException = (RollbackException) exception.getCause();

            if (rollbackException.getCause() instanceof ConstraintViolationException) {
                body.put("timestamp", LocalDateTime.now());
                body.put("message", ((ConstraintViolationException) rollbackException.getCause()).getConstraintViolations());

                return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
            }
        }

        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {InvalidFieldsException.class})
    protected ResponseEntity<Object> handleInvalidFieldsException(InvalidFieldsException exception, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());
        log.error("handleInvalidFieldsException", exception);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {InvalidFileException.class})
    protected ResponseEntity<Object> handleInvalidFileException(InvalidFileException exception, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());
        log.error("handleInvalidFileException",exception);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException exception) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());
        log.error("handleEntityNotFoundException",exception);

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(FailToSendEmail.class)
    public ResponseEntity<Map<String, Object>> handleFailSendMessage(FailToSendEmail exception) {
        Map<String, Object> response = new HashMap<>();
        if (exception.getTraining().isPresent()) {
            ObjectMapper jsonMapper = new ObjectMapper();
            response = jsonMapper.convertValue(exception.getTraining().get(), Map.class);
        }
        response.put("failToSend", exception.getFailToSend());
        return new ResponseEntity<>(response, HttpStatus.MULTI_STATUS);
    }
    @ExceptionHandler(InvalidPageException.class)
    public ResponseEntity<Object> handleInvalidPageException(InvalidPageException exception){
        Map<String, Object> body = new HashMap<>();

        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException exception,
                                                               HttpHeaders headers,
                                                               HttpStatus status,
                                                               WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());
        log.error("HttpMessageNotReadableException", exception);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundEmployeesException.class)
    public ResponseEntity<Object> handleNotFoundEmployeesException(NotFoundEmployeesException exception) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("employees", exception.getEmployees());

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler({InvalidStringException.class, RuntimeException.class})
    public ResponseEntity<Object> handleGenericException(RuntimeException exception) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());
        log.error("Generic handler", exception);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(LoginException.class)
    public ResponseEntity<Object> handleLoginException(LoginException exception) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", exception.getMessage());

        return new ResponseEntity<>(body, HttpStatus.UNAUTHORIZED);
    }
}
