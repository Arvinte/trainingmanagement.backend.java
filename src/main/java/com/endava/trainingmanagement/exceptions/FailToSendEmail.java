package com.endava.trainingmanagement.exceptions;

import com.endava.trainingmanagement.dto.TrainingDto;

import java.util.List;
import java.util.Optional;

public class FailToSendEmail extends RuntimeException {
    private final List<String> failToSend;
    private TrainingDto training;

    public FailToSendEmail(List<String> failToSend) {
        this.failToSend = failToSend;
    }

    public FailToSendEmail(List<String> failToSend, TrainingDto training) {
        this.failToSend = failToSend;
        this.training = training;
    }

    public List<String> getFailToSend() {
        return failToSend;
    }

    public Optional<TrainingDto> getTraining() {
        return Optional.ofNullable(training);
    }
}
