package com.endava.trainingmanagement.exceptions;

public class InvalidFieldsException extends Exception {
    public InvalidFieldsException(String message) {
        super(message);
    }
}
