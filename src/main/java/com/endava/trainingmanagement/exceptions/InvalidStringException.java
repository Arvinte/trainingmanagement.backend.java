package com.endava.trainingmanagement.exceptions;

public class InvalidStringException extends RuntimeException {
    public InvalidStringException(String message) {
        super(message);
    }
}
