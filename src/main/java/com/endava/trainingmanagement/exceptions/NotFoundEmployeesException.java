package com.endava.trainingmanagement.exceptions;

import lombok.Getter;

import java.util.List;

@Getter
public class NotFoundEmployeesException extends RuntimeException {
    private List<String> employees;

    public NotFoundEmployeesException(List<String> employees) {
        this.employees = employees;
    }
}
