package com.endava.trainingmanagement.exceptions;

public class LoginException extends RuntimeException {

    public LoginException(String email) {
        super("The email " + email + " is not registered in the system");
    }
}
