package com.endava.trainingmanagement.exceptions;

public class InvalidPageException extends RuntimeException{
    public InvalidPageException(String message) {
        super(message);
    }
}
