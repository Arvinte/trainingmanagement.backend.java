package com.endava.trainingmanagement.exceptions;

public class InvalidFileException extends Exception {
    public InvalidFileException(String message) {
        super(message);
    }
}
