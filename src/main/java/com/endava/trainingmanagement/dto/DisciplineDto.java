package com.endava.trainingmanagement.dto;

import com.endava.trainingmanagement.constraint.DisciplineNameConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DisciplineDto {
    private Long id;

    @DisciplineNameConstraint
    private String name;
}
