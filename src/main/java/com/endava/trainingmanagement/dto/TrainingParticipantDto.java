package com.endava.trainingmanagement.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class TrainingParticipantDto {
    @NotNull
    private EmployeeDto employee;

    @NotNull
    private String status;
}
