package com.endava.trainingmanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilterTrainingReportResultDTO {
    private String trainingName;
    private List<String> trainers;
    private String trainee;
    private String traineeStatus;
    private String discipline;
    private List<String> skills;
    private String startDate;
    private String endDate;
}
