package com.endava.trainingmanagement.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class FilterTrainingResultDto {
    private long totalResults;
    private List<TrainingDto> results;
}
