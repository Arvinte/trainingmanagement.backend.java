package com.endava.trainingmanagement.dto;

import com.endava.trainingmanagement.constraint.NameConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
public class EmployeeDto {
    private Long id;

    @NameConstraint
    private String firstName;

    @NameConstraint
    private String lastName;

    @Email
    private String email;

    private DisciplineDto discipline;
}
