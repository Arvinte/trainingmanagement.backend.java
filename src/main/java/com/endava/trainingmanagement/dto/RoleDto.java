package com.endava.trainingmanagement.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class RoleDto {
    @NotNull
    private Long id;

    @NotNull
    private String name;
}
