package com.endava.trainingmanagement.dto;

import com.endava.trainingmanagement.constants.ValidationConstants;
import com.endava.trainingmanagement.constraint.LocationConstraint;
import com.endava.trainingmanagement.constraint.MeetingUrlConstraint;
import com.endava.trainingmanagement.constraint.ParticipantsConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class TrainingDto {
    private Long id;

    @Valid
    @NotNull
    private Set<EmployeeDto> trainers = new HashSet<>();

    @ParticipantsConstraint
    private Set<String> participants = new HashSet<>();

    @Valid
    private Set<SkillDto> skills = new HashSet<>();

    @Valid
    private DisciplineDto discipline;

    @NotNull
    @Size(max = ValidationConstants.TRAINING_NAME_MAX_LENGTH)
    private String name;

    @Valid
    @LocationConstraint
    private String location;

    private String description;

    private Timestamp startDate;

    private Timestamp endDate;

    @NotNull
    private Boolean isInternal;

    @MeetingUrlConstraint
    private String meetingURL;

    private String icsFilename;

    @NotNull
    private boolean sendInvite;
}
