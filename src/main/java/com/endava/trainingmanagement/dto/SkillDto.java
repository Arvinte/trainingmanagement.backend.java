package com.endava.trainingmanagement.dto;

import com.endava.trainingmanagement.constants.ValidationConstants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SkillDto {
    private Long id;

    @NotNull
    @Size(max = ValidationConstants.SKILL_NAME_MAX_LENGTH)
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SkillDto skillDto = (SkillDto) o;
        return getName().equals(skillDto.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
