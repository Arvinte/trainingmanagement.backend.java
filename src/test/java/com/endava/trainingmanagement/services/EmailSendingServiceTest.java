package com.endava.trainingmanagement.services;

import com.endava.trainingmanagement.config.EmailConfigurationTest;
import com.endava.trainingmanagement.email.ConfirmationMessage;
import com.endava.trainingmanagement.email.InvitationMessage;
import com.endava.trainingmanagement.service.EmailSendingService;
import com.endava.trainingmanagement.utils.EmailMapper;
import com.icegreen.greenmail.store.FolderException;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import org.assertj.core.api.Assertions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileCopyUtils;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;


@ContextConfiguration(classes = {EmailConfigurationTest.class})
@ActiveProfiles("test-email")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EmailSendingService.class, EmailMapper.class, SpringTemplateEngine.class})
public class EmailSendingServiceTest {
    @Autowired
    public EmailSendingService emailSendingService;

    @Autowired
    ResourceLoader resourceLoader;

    public static GreenMail testServerSmtp;
    public static String urlTest;

    @BeforeClass
    public static void testSmtpInit() throws IOException {
        testServerSmtp = new GreenMail(ServerSetupTest.SMTP);
        testServerSmtp.start();
        urlTest = "www.test.com";
    }

    @AfterClass
    public static void cleanup() {
        testServerSmtp.stop();
    }

    @BeforeEach
    public void removeMessages() throws FolderException {
        testServerSmtp.purgeEmailFromAllMailboxes();
    }

    @Test
    public void sendingInvitationEmail_shouldReceiveCorrectEmail_whenEmailIsSend() throws MessagingException, IOException {
        Resource resource = resourceLoader.getResource("classpath:test.ics");
        InputStream inputStream = resource.getInputStream();
        byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
        InvitationMessage request = new InvitationMessage();
        request.setName("John");
        request.setDate(LocalDate.of(2010, 12, 12));
        request.setTrainingName("Java");

        emailSendingService.sendEmail("test@recevier.com", request, bdata, "calendar");

        MimeMessage[] messages = testServerSmtp.getReceivedMessages();
        int position = messages.length - 1;
        String body = GreenMailUtil.getBody(messages[position]).replaceAll("=\r?\n", "");
        String contentType = messages[position].getContentType();
        Multipart mail = (Multipart) messages[position].getContent();

        Assertions.assertThat(messages[position].getSubject()).isEqualTo("Invitation training");
        Assertions.assertThat(contentType).contains("multipart/mixed");
        Assertions.assertThat(mail.getCount()).isEqualTo(2);
        Assertions.assertThat(body)
                .contains("Java")
                .contains("John")
                .contains("12/12/2010");
    }

    @Test
    public void sendingConfirmationEmail_shouldReceiveCorrectEmail_whenEmailIsSend() throws MessagingException {

        ConfirmationMessage request = new ConfirmationMessage();
        request.setNameParticipant("John");
        request.setTrainingName("Java");
        request.setMeetingUrl(urlTest);
        request.setDateOfTraining(urlTest + 1);
        emailSendingService.sendEmail("test@recevier.com", request);

        Message[] messages = testServerSmtp.getReceivedMessages();
        int position = messages.length - 1;
        String body = GreenMailUtil.getBody(messages[position]).replaceAll("=\r?\n", "");

        Assertions.assertThat(messages[position].getSubject()).isEqualTo("Confirmed training details");
        Assertions.assertThat(body)
                .contains("Java")
                .contains("John")
                .contains(urlTest)
                .contains(urlTest + 1);
    }
}
