package com.endava.trainingmanagement.services;

import com.endava.trainingmanagement.model.TrainingParticipant;
import com.endava.trainingmanagement.repository.TrainingParticipantRepository;
import com.endava.trainingmanagement.service.NotificationService;
import com.endava.trainingmanagement.service.StatusParticipationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class StatusParticipantsServiceTest {

    private static StatusParticipationService service;
    private static HttpServletResponse response;
    private static TrainingParticipantRepository repository;
    private static List<TrainingParticipant> data;

    @BeforeClass
    public static void init() throws IOException {
        setUpMock();
        mockRepo();
        data = getMockedData();
        response = new MockHttpServletResponse();
    }

    @Test
    public void updateConfirmationStatus_shouldReturnErrorRedirect_whenCodeIsNotFound() {
        UUID code = UUID.fromString("7c3f365c-f4f2-4a89-8be5-380d96d920e1");
        service.updateStatus(code, response);
        String expected = "https://testHost/error/7c3f365c-f4f2-4a89-8be5-380d96d920e1?trainingName=";
        Assertions.assertThat(response.getHeader("Location")).isEqualTo(expected);
    }

    @Test
    public void updateConfirmationStatus_shouldReturnConfirmationRedirect_whenCodeIsValidAndNotExpired() {
        UUID code = UUID.fromString("7c3f365c-f4f2-4a89-8be5-380d96d920ea");
        service.updateStatus(code, response);
        String expected = "https://testHost/confirmation/7c3f365c-f4f2-4a89-8be5-380d96d920ea?trainingName=Trainng";
        Assertions.assertThat(response.getHeader("Location")).isEqualTo(expected);
    }

    private static void setUpMock() {
        repository = mock(TrainingParticipantRepository.class);
        NotificationService notificationService = mock(NotificationService.class);

        doNothing().when(notificationService).sendDetailsEmail(any());
        response = mock(HttpServletResponse.class);
        service = new StatusParticipationService(repository, notificationService);
        service.setHostUrlRedirect("https://testHost");
        service.setSecondsToExpire(4000);

    }

    private static void mockRepo() {
        when(repository.findByInvitationCode(any()))
                .thenAnswer((Answer<Optional<TrainingParticipant>>) invocation -> {
                    UUID invocationArgument = (UUID) invocation.getArguments()[0];
                    return data.stream()
                            .filter(e -> e.getInvitationCode().equals(invocationArgument))
                            .findFirst();
                });
        when(repository.save(any())).thenReturn(null);
    }

    private static List<TrainingParticipant> getMockedData() throws IOException {
        Path resourceDirectory = Paths.get("src", "test", "resources", "mocks", "training_participants_confirmation.json");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        String data = Files.readString(Paths.get(absolutePath));
        List<TrainingParticipant> trainingParticipants = List.of(new ObjectMapper().readValue(data, TrainingParticipant[].class));
        trainingParticipants.forEach(participant -> participant.setModificationDate(Timestamp.from(Instant.now())));
        return trainingParticipants;
    }
}


