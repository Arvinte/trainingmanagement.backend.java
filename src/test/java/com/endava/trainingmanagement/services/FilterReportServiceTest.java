package com.endava.trainingmanagement.services;

import com.endava.trainingmanagement.service.FilterReportsService;
import com.endava.trainingmanagement.utils.ReportFilter;
import org.assertj.core.api.Assertions;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class FilterReportServiceTest {
    @Autowired
    private FilterReportsService trainingService;

    @Autowired
    Flyway flyway;

    @BeforeEach
    public void beforeEach() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void getTrainingsForReport_shouldReturnData_whenNoFilterIsPresent() {
        //given
        int expectedNumberOfResults = 106;
        ReportFilter reportFilter = new ReportFilter();

        //when
        int actualNumberOfResults = trainingService.getReportSize(reportFilter);

        //that
        Assertions.assertThat(actualNumberOfResults).isEqualTo(expectedNumberOfResults);
    }

    @Test
    public void getTrainingsForReport_shouldReturnData_whenNameFilterIsPresent() {
        //given
        int expectedNumberOfResults = 24;
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setNames(Arrays.asList("Training 1", "Training 2"));

        //when
        int actualNumberOfResults = this.trainingService.getReportSize(reportFilter);

        //that
        Assertions.assertThat(actualNumberOfResults).isEqualTo(expectedNumberOfResults);
    }

    @Test
    public void getTrainingsForReport_shouldReturnData_whenNameFilterAndConfirmedStatusRequiredPresent() {
        //given
        int expectedNumberOfResults = 11;
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setNames(Arrays.asList("Training 1", "Training 2"));
        reportFilter.setTraineeStatus(Arrays.asList("Confirmed"));

        //when
        int actualNumberOfResults = this.trainingService.getReportSize(reportFilter);

        //that
        Assertions.assertThat(actualNumberOfResults).isEqualTo(expectedNumberOfResults);
    }

    @Test
    public void getTrainingsForReport_shouldReturnData_whenNameFilterAndConfirmedInvitedStatusRequiredPresent() {
        //given
        int expectedNumberOfResults = 30;
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setNames(Arrays.asList("Training 1", "Training 32"));
        reportFilter.setTraineeStatus(Arrays.asList("Confirmed", "Declined"));

        //when
        int actualNumberOfResults = this.trainingService.getReportSize(reportFilter);

        //that
        Assertions.assertThat(actualNumberOfResults).isEqualTo(expectedNumberOfResults);
    }

    @Test
    public void getTrainingsForReport_shouldReturnData_whenNameFilter_AndConfirmedDeclinedStatus_AndTraineeNameIsPresent() {
        //given
        int expectedNumberOfResults = 2;
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setNames(Arrays.asList("Training 1", "Training 32"));
        reportFilter.setTraineeStatus(Arrays.asList("Confirmed", "Declined"));
        reportFilter.setTraineeName(Arrays.asList("Gheorghidiu"));

        //when
        int actualNumberOfResults = this.trainingService.getReportSize(reportFilter);

        //that
        Assertions.assertThat(actualNumberOfResults).isEqualTo(expectedNumberOfResults);
    }

    @Test
    public void getTrainingsForReport_shouldReturnData_whenNameFilter_AndConfirmedDeclinedStatus_And2TraineeNameIsPresent() {
        //given
        int expectedNumberOfResults = 4;
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setNames(Arrays.asList("Training 1", "Training 32"));
        reportFilter.setTraineeStatus(Arrays.asList("Confirmed", "Declined"));
        reportFilter.setTraineeName(Arrays.asList("Gheorghidiu", "Iordache"));

        //when
        int actualNumberOfResults = this.trainingService.getReportSize(reportFilter);

        //that
        Assertions.assertThat(actualNumberOfResults).isEqualTo(expectedNumberOfResults);
    }

    @Test
    public void getTrainingsForReport_shouldReturnData_whenNameFilter_AndConfirmedDeclinedStatus_And2TraineeName_AndJavaSkillIsPresent() {
        //given
        int expectedNumberOfResults = 0;
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setNames(Arrays.asList("Training 1", "Training 32"));
        reportFilter.setTraineeStatus(Arrays.asList("Confirmed", "Declined"));
        reportFilter.setTraineeName(Arrays.asList("Gheorghidiu", "Iordache"));
        reportFilter.setSkills(Arrays.asList("JAVA"));

        //when
        int actualNumberOfResults = this.trainingService.getReportSize(reportFilter);

        //that
        Assertions.assertThat(actualNumberOfResults).isEqualTo(expectedNumberOfResults);
    }

}
