package com.endava.trainingmanagement.services;

import com.endava.trainingmanagement.constants.StatusInvitation;
import com.endava.trainingmanagement.email.EmailMessage;
import com.endava.trainingmanagement.email.InvitationMessage;
import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.model.TrainingParticipant;
import com.endava.trainingmanagement.repository.TrainingRepository;
import com.endava.trainingmanagement.service.EmailSendingService;
import com.endava.trainingmanagement.service.NotificationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.stubbing.Answer;

import javax.mail.MessagingException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class NotificationServiceTest {

    private static EmailSendingService emailSendingService;
    private static TrainingRepository trainingRepository;
    private static Training training;


    @BeforeClass
    public static void init() throws IOException, MessagingException {
        setUpMocks();
        mockUpEmailSendingService();
        mockUpRepositorySaveMethod();
        training = getMockedData();

        NotificationService notificationService = new NotificationService(emailSendingService, trainingRepository, "http://localhost:8080");
        notificationService.sendInvitationForParticipants(training);
    }


    @Test
    public void sendingInvitationNotification_shouldHaveStatusModifiedInDBForEachParticipant_whenTrainingIdIs1() {
        for (TrainingParticipant participant : training.getParticipants()) {
            Assertions.assertThat(participant.getInvitationCode()).isNotNull();
            Assertions.assertThat(participant.getModificationDate()).isNotNull();
            Assertions.assertThat(participant.getStatus()).isEqualTo(StatusInvitation.Invited);
        }
    }

    @Test
    public void sendingInvitationNotification_shouldSendAEmailForEachParticipant_whenTrainingIdIs1() throws MessagingException {
        ArgumentCaptor<String> acEmail = ArgumentCaptor.forClass(String.class);
        verify(emailSendingService, times(3))
                .sendEmail(acEmail.capture(), any(EmailMessage.class), any(byte[].class),any(String.class));

        List<String> expectedEmails = training.getParticipants()
                .stream()
                .map(e -> e.getEmployee().getEmail())
                .collect(Collectors.toList());

        Assertions.assertThat(acEmail.getAllValues()).isEqualTo(expectedEmails);
    }

    @Test
    public void sendingInvitationNotification_shouldSendACustomEmailForEachParticipant_whenTrainingIdIs1() throws MessagingException {
        ArgumentCaptor<InvitationMessage> acInvitation = ArgumentCaptor.forClass(InvitationMessage.class);
        verify(emailSendingService, times(3))
                .sendEmail(anyString(), acInvitation.capture());

        List<String> expectedEmails = training.getParticipants()
                .stream()
                .map(e -> e.getEmployee().getFirstName())
                .collect(Collectors.toList());

        boolean anyParticipantsHaveConfirmationCode = training.getParticipants()
                .stream()
                .map(TrainingParticipant::getInvitationCode)
                .allMatch(Objects::nonNull);

        List<String> actualEmails = acInvitation.getAllValues()
                .stream()
                .map(InvitationMessage::getName)
                .collect(Collectors.toList());

        Assertions.assertThat(actualEmails).isEqualTo(expectedEmails);
        Assertions.assertThat(anyParticipantsHaveConfirmationCode).isTrue();
    }

    private static void mockUpRepositorySaveMethod() {
        when(trainingRepository.save(any(Training.class)))
                .thenAnswer((Answer<Training>) invocation -> {
                    Object[] args = invocation.getArguments();
                    training = (Training) args[0];
                    return training;
                });
    }

    private static void mockUpEmailSendingService() throws MessagingException {
        emailSendingService = mock(EmailSendingService.class);
        doNothing().when(emailSendingService).sendEmail(anyString(), any(EmailMessage.class));
    }


    private static void setUpMocks() {
        emailSendingService = mock(EmailSendingService.class);
        trainingRepository = mock(TrainingRepository.class);
    }

    private static Training getMockedData() throws IOException {
        Path resourceDirectory = Paths.get("src", "test", "resources", "mocks", "trainings-test-notification.json");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        String data = Files.readString(Paths.get(absolutePath));
        return new ObjectMapper().readValue(data, Training.class);
    }


}
