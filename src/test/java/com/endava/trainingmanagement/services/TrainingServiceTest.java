package com.endava.trainingmanagement.services;

import com.endava.trainingmanagement.dto.TrainingDto;
import com.endava.trainingmanagement.helpers.TestResourceResolver;
import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.service.TrainingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.annotation.DirtiesContext;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TrainingServiceTest {

    @Autowired
    private TrainingService trainingService;

    private static List<Training> mockedTrainings;

    @BeforeAll
    static void init() throws FileNotFoundException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String content = TestResourceResolver.readFromFile("src/test/resources/trainings_mock.json");
        mockedTrainings = objectMapper.readValue(content, new TypeReference<>() {
        });
    }

    @Test
    void getByPage_ShouldReturn2Elements_WhenPageNumberIs2_AndPageSizeIs2() {
        //given
        List<String> expectedTrainingNames = Arrays.asList("Third training", "Fourth training");

        //when
        List<String> actualTrainingNames = trainingService.getByPage(mockedTrainings, 2L, 2).stream()
                .map(TrainingDto::getName)
                .collect(Collectors.toList());

        //that
        Assertions.assertThat(actualTrainingNames).containsAll(expectedTrainingNames);

    }

    @Test
    void getByPage_shouldReturnLastElementWhen_pageNumberIs3_AndPageSizeIs2() {
        //given
        List<String> expectedTrainingName = Collections.singletonList("Fifth training");

        //when
        List<String> actualTrainingName = trainingService.getByPage(mockedTrainings, 3L, 2).stream()
                .map(TrainingDto::getName)
                .collect(Collectors.toList());
        //that
        Assertions.assertThat(actualTrainingName).containsAll(expectedTrainingName);

    }

    @Test
    void getByPage_shouldReturnEmptyList_whenPageNumberIsTooHigh() {
        //given
        int expectedListSize = 0;

        //when
        int actualListSize = trainingService.getByPage(mockedTrainings, 100L, 2).size();

        //that
        Assertions.assertThat(actualListSize).isEqualTo(expectedListSize);
    }

}
