package com.endava.trainingmanagement.services;

import com.endava.trainingmanagement.exceptions.NotFoundEmployeesException;
import com.endava.trainingmanagement.model.Employee;
import com.endava.trainingmanagement.repository.EmployeeRepository;
import com.endava.trainingmanagement.service.EmployeeService;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public  class EmployeeServiceTest {
    private static EmployeeService employeeService;
    private static EmployeeRepository employeeRepository;
    private static List<Employee> employees;

    @BeforeClass
    public static void init() throws IOException {
        employeeRepository = mock(EmployeeRepository.class);
        employeeService = new EmployeeService(employeeRepository);

        employees = getEmployeesData();

        mockUpRepositoryExists();
    }

    @Test
    public void getExistsEmployees_ShouldReturnStatusOk_WhenEmployeesExists() throws IOException {
        ResponseEntity<Object> response = employeeService.existsEmployees(List.of("Prenume1 Nume1", "Prenume3 Nume3"));
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void getExistsEmployees_ShouldReturnNotFound_WhenOneEmployeeDoesntExists() throws IOException {
        NotFoundEmployeesException exception = Assertions.catchThrowableOfType(() ->
                employeeService.existsEmployees(List.of("Prenume1 Nume1", "invalid name")),
                NotFoundEmployeesException.class);
        Assertions.assertThat(exception.getEmployees()).contains("invalid name");
    }

    private static List<Employee> getEmployeesData() throws IOException {
        Path resourceDirectory = Paths.get("src", "test", "resources", "employees_mock.json");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        String data = Files.readString(Paths.get(absolutePath));
        return List.of(new ObjectMapper()
                .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
                .readValue(data, Employee[].class));
    }

    private static void mockUpRepositoryExists() {
        when(employeeRepository.findByFirstNameAndLastName("Prenume1 Nume1"))
                .thenAnswer((Answer<Optional<Employee>>) invocation -> {
                    return Optional.of(employees.get(0));
                });

        when(employeeRepository.findByFirstNameAndLastName("Prenume2 Nume2"))
                .thenAnswer((Answer<Optional<Employee>>) invocation -> {
                    return Optional.of(employees.get(1));
                });

        when(employeeRepository.findByFirstNameAndLastName("Prenume3 Nume3"))
                .thenAnswer((Answer<Optional<Employee>>) invocation -> {
                    return Optional.of(employees.get(3));
                });

        when(employeeRepository.findByFirstNameAndLastName("Prenume4 Nume4"))
                .thenAnswer((Answer<Optional<Employee>>) invocation -> {
                    return Optional.of(employees.get(3));
                });
    }
}