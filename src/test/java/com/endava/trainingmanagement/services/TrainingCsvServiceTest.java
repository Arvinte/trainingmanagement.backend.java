package com.endava.trainingmanagement.services;

import com.endava.trainingmanagement.exceptions.EntityNotFoundException;
import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.repository.TrainingRepository;
import com.endava.trainingmanagement.service.FilterReportsService;
import com.endava.trainingmanagement.service.TrainingCsvService;
import com.endava.trainingmanagement.service.TrainingService;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.stubbing.Answer;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TrainingCsvServiceTest {
    private static TrainingRepository trainingRepository;
    private static TrainingCsvService trainingCsvService;
    private static FilterReportsService filterReportsService;
    private static TrainingService trainingService;
    private static Training training;

    @BeforeClass
    public static void init() throws IOException {
        trainingRepository = mock(TrainingRepository.class);
        filterReportsService = mock(FilterReportsService.class);
        trainingService = mock(TrainingService.class);
        trainingCsvService = new TrainingCsvService(trainingRepository, filterReportsService, trainingService);

        training = getMockedData();

        mockUpRepositoryFindByIdMethod();
    }

    @Test
    public void getParticipantsCsv_ShouldReturnAValidFile_WhenIdIsOne() throws IOException {
        ResponseEntity<ByteArrayResource> response = trainingCsvService.getTrainingParticipantsCsv(1L);
        Set<String> mockedCsv = Set.of(new String(response.getBody().getByteArray()).split("\n"));
        Set<String> fileCsv = Set.of(getMockFile("test-participants.csv").split("\n"));

        Assertions.assertThat(mockedCsv.containsAll(fileCsv)).isTrue();
        ContentDisposition contentDisposition = response.getHeaders().getContentDisposition();
        Assertions.assertThat(contentDisposition.getFilename()).isEqualTo("Training test.csv");
    }

    @Test(expected = EntityNotFoundException.class)
    public void getParticipantsCsv_ShouldReturnNotFound_WhenIdIsInvalid() {
        trainingCsvService.getTrainingParticipantsCsv(2L);
    }

    private static void mockUpRepositoryFindByIdMethod() {
        when(trainingRepository.findById(1L))
                .thenAnswer((Answer<Optional<Training>>) invocation -> {
                    return Optional.of(training);
                });
    }

    private static Training getMockedData() throws IOException {
        String data = getMockFile("training-test-participants-csv.json");
        return new ObjectMapper()
                .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
                .readValue(data, Training.class);
    }

    private static String getMockFile(String filename) throws IOException {
        Path resourceDirectory = Paths.get("src", "test", "resources", "mocks", filename);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        return Files.readString(Paths.get(absolutePath));
    }
}
