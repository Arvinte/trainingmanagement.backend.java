package com.endava.trainingmanagement.helpers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.stream.Collectors;

public class TestResourceResolver {
    public static String readFromFile(String path) throws FileNotFoundException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
        return bufferedReader.lines()
                .collect(Collectors.joining());
    }
}
