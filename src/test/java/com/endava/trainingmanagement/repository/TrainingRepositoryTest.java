package com.endava.trainingmanagement.repository;

import com.endava.trainingmanagement.TrainingManagementApplication;
import com.endava.trainingmanagement.helpers.TestResourceResolver;
import com.endava.trainingmanagement.model.Training;
import com.endava.trainingmanagement.utils.Filter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.bind.v2.runtime.output.SAXOutput;
import org.apache.tomcat.jni.Local;
import org.assertj.core.api.Assertions;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.sql.Array;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TrainingRepositoryTest {
    @Autowired
    Flyway flyway;

    @Autowired
    private TrainingRepository trainingRepository;

    private static List<Training> mockedTrainings;

    @BeforeEach
    public void beforeEach() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void GetAllTrainingsShouldNotReturnEmptyList() {
        //when
        int actualNumberOfTrainings = trainingRepository.findAll().size();

        //that
        Assertions.assertThat(actualNumberOfTrainings).isNotZero();
    }

    @Test
    public void ShouldSaveATraining() {
        //given
        Training newTraining = new Training();
        newTraining.setName("Newest Java Training");
        newTraining.setLocation("iasi");
        newTraining.setStartDate(Timestamp.from(Instant.now()));
        newTraining.setEndDate(Timestamp.from(Instant.now()));
        newTraining.setMeetingURL("meeting");
        newTraining.setCreatedAt(Timestamp.from(Instant.now()));
        newTraining.setEditedAt(Timestamp.from(Instant.now()));
        //when
        trainingRepository.save(newTraining);

        //that
        Assertions.assertThat(newTraining.getId()).isNotNull();
    }

    @Test
    public void ShouldGetTrainingByIDWhenTrainingExists() {
        //given
        int searchedTraining = 1;

        //when
        Optional optional = trainingRepository.findById(Long.valueOf(searchedTraining));

        //that
        Assertions.assertThat(optional).isNotEmpty();
    }

    @Test
    public void ShouldNotGetTrainingWhenTrainingIdDoesNotExists() {
        //given
        int searchedTraining = 1000;

        //when
        Optional optional = trainingRepository.findById(Long.valueOf(searchedTraining));

        //that
        Assertions.assertThat(optional).isEmpty();
    }

    @Test
    @Transactional
    public void ShouldUpdateTrainingField() {
        //given
        String newTrainingName = "new Training name";
        Long trainingIdOfTrainingToBeUpdated = 1l;

        //when
        Training trainingToBeUpdated = trainingRepository.getOne(trainingIdOfTrainingToBeUpdated);
        trainingToBeUpdated.setName(newTrainingName);
        trainingRepository.save(trainingToBeUpdated);

        //that
        Assertions.assertThat(trainingRepository.getOne(trainingIdOfTrainingToBeUpdated).getName()).isEqualTo(newTrainingName);

    }

    //
    @Test
    public void filterTrainings_shouldReturnAllTrainings_whenNoFilterIsPresent() {
        //given
        Filter filter = new Filter();
        int expectedNumberOfResults = 57;
        //when
        long actualNumberOfResults = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();
        //that
        Assertions.assertThat(expectedNumberOfResults).isEqualTo(actualNumberOfResults);
    }

    @Test
    public void filterTrainings_shouldReturnTrainings_WhenNameFilterIsPresent() {
        //given
        Filter filter = new Filter();
        filter.setNames(Arrays.asList("Training 1"));
        filter.toLowerCase();
        int expectedNumberOfTrainings = 1;

        //when
        int actualNumberOfTrainings = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();

        //that
        Assertions.assertThat(expectedNumberOfTrainings).isEqualTo(actualNumberOfTrainings);
    }

    @Test
    public void filterTrainings_shouldReturnTrainings_When2NameFilterIsPresent() {
        //given
        Filter filter = new Filter();
        filter.setTrainers(Arrays.asList("Ariana", "Evelina"));
        filter.toLowerCase();

        int expectedNumberOfTrainings = 16;

        //when
        int actualNumberOfTrainings = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();
        //that
        Assertions.assertThat(expectedNumberOfTrainings).isEqualTo(actualNumberOfTrainings);
    }

    @Test
    public void filterTrainings_shouldReturnTrainingsWhen2NameFilterAndDisciplineFilterIsPresent() {
        //given
        Filter filter = new Filter();
        filter.setTrainers(Arrays.asList("Evelina", "Ariana"));
        filter.setDiscipline("Development");
        filter.toLowerCase();

        int expectedNumberOfTrainings = 3;

        //when
        int actualNumberOfTrainings = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();
        //that
        Assertions.assertThat(expectedNumberOfTrainings).isEqualTo(actualNumberOfTrainings);
    }

    @Test
    public void filterTraining_shouldReturnNoTraining_When2NameFilterIsPresentAndDateFilterIsPresent() {
        Filter filter = Filter.builder()
                .trainers(Arrays.asList("George", "Enache"))
                .startDate(LocalDate.of(2020, 1, 12))
                .endDate(LocalDate.of(2020, 1, 13))
                .build();
        filter.toLowerCase();

        int expectedNumberOfTrainings = 1;

        //when
        int actualNumberOfTrainings = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();
        //that
        Assertions.assertThat(expectedNumberOfTrainings).isEqualTo(actualNumberOfTrainings);
    }

    @Test
    public void filterTrainings_shouldReturnATraining_WhenDateFilterIsPresent() {
        //given
        Filter filter = Filter.builder()
                .startDate(LocalDate.of(2021, 10, 11))
                .endDate(LocalDate.of(2022, 10, 12))
                .build();
        filter.toLowerCase();

        int expectedNumberOfTrainings = 1;

        //when
        int actualNumberOfTrainings = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();

        //that
        Assertions.assertThat(expectedNumberOfTrainings).isEqualTo(actualNumberOfTrainings);
    }

    @Test
    public void filterTrainings_shouldReturnTrainings_whenDateFilterIsPresent() {
        //given
        Filter filter = Filter.builder()
                .startDate(LocalDate.of(2020, 9, 13))
                .endDate(LocalDate.of(2020, 11, 30))
                .build();
        filter.toLowerCase();

        int expectedNumberOfTrainings = 12;

        //when
        int actualNumberOfTrainings = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();

        //that
        Assertions.assertThat(expectedNumberOfTrainings).isEqualTo(actualNumberOfTrainings);
    }

    @Test
    public void filterTrainings_shouldReturnTrainings_whenNameFilterIsPresent() {
        //given
        Filter filter = new Filter();
        filter.setNames(Arrays.asList("Training 1", "Training 2"));
        filter.toLowerCase();

        int expectedNumberOfTrainings = 2;

        //when
        int actualNumberOfTrainings = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();
        //that
        Assertions.assertThat(expectedNumberOfTrainings).isEqualTo(actualNumberOfTrainings);
    }

    @Test
    public void filterTraining_shouldReturnATraining_WhenNameAndDisciplineFilterIsPresent() {
        Filter filter = new Filter();
        filter.setNames(Arrays.asList("Training 1", "Training 2"));
        filter.setDiscipline("Development");
        filter.toLowerCase();

        int expectedNumberOfTrainings = 1;
        //when
        int actualNumberOfTrainings = trainingRepository.filterTrainings(filter.getNames(), filter.getSkills(),
                filter.getLocations(), filter.getDiscipline(), filter.getTrainers(),
                filter.getEndDate(), filter.getStartDate()).size();
        //that
        Assertions.assertThat(expectedNumberOfTrainings).isEqualTo(actualNumberOfTrainings);
    }
}
