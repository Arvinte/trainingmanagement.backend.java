package com.endava.trainingmanagement.controller;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.util.UUID;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ParticipationValidationControllerIT {
    private static RestTemplate restTemplate;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:8080/validation"));
    }

    @Test
    public void isValid_shouldReturnTrue_whenParticipantWithInvitationCodeExists() {
        //given
        UUID validationCode = UUID.fromString("28a92282-a527-4ff8-b7e9-4d857375d0ad");

        //when
        ResponseEntity<Boolean> responseEntity = restTemplate.getForEntity("/" + validationCode, Boolean.class);

        //that
        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(responseEntity.getBody()).isEqualTo(true);
    }

    @Test
    public void isValid_shouldReturnFalse_whenParticipantWithInvitationCodeDontExist() {
        //given
        UUID validationCode = UUID.fromString("22292282-2527-2ff8-b2e9-4d857375d0ab");

        //when
        ResponseEntity<Boolean> responseEntity = restTemplate.getForEntity("/" + validationCode, Boolean.class);

        //that
        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(responseEntity.getBody()).isEqualTo(false);
    }
}
