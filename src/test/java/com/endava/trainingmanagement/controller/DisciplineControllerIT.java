package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.dto.DisciplineDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.util.List;
import java.util.Objects;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DisciplineControllerIT {
    private static RestTemplate restTemplate;

    @BeforeAll
    static void init() {
        restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:8080"));
    }

    @Test
    void getAll_shouldNotReturnAnEmptyList() {
        List<DisciplineDto> disciplines = restTemplate.getForObject("/disciplines", List.class);
        Assertions.assertThat(Objects.nonNull(disciplines));
        Assertions.assertThat(disciplines.size()).isNotZero();
    }
}
