package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.TrainingManagementApplication;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.util.List;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class EmployeeControllerIT {
    private static RestTemplate restTemplate;
    @Autowired
    Flyway flyway;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:8080/employees"));
    }

    @BeforeEach
    public void cleanup() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    void getExists_ShouldReturnOk_WhenAllEmployeesExists() {
        var response = restTemplate.exchange("/exists?list=George Enache,Andrei Lungeanu,Cornelia Ene",
                HttpMethod.GET, new HttpEntity<>(null,new HttpHeaders()), Void.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void getExists_ShouldReturnBadRequest_WhenOneEmployeeDoesntExist() throws JsonProcessingException {
        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.getForObject("/exists?list=George Enache,Andrei Lungeanu,invalid", Void.class),
                HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        Map value = new ObjectMapper().readValue(exception.getResponseBodyAsString(), Map.class);
        Assertions.assertThat(value.get("employees")).isEqualTo(List.of("invalid"));
    }
}
