package com.endava.trainingmanagement.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class HealthCheckControllerTest {
    private static RestTemplate restTemplate;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
    }

    @Test
    public void shouldCheckHealth() {
        String result = restTemplate.getForObject("http://localhost:8080/health", String.class);
        Assertions.assertEquals("This application doesn't have covid-19", result);
    }

}
