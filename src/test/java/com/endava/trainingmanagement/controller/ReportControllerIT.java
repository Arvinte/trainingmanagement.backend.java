package com.endava.trainingmanagement.controller;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ReportControllerIT {
    private static RestTemplate restTemplate;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:8080/reports"));
    }

    @Test
    public void getEmployeesReport_ShouldReturnOk_WhenEmployeeListIsNotEmpty() {
        String reportTitle = "Training name,Trainer,Trainee,Trainee Status,Skills,Discipline,Start,End\n";
        ResponseEntity<ByteArrayResource> response = restTemplate.getForEntity("/employees?discipline=Development", ByteArrayResource.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(new String(Objects.requireNonNull(response.getBody()).getByteArray(), StandardCharsets.UTF_8))
                .isNotEqualTo(reportTitle);
    }

    @Test
    public void getTrainingsReport_ShouldReturnOk_WhenEmployeesListIsEmpty() {
        String reportTitle = "Training name,Trainer,Trainee,Trainee Status,Skills,Discipline,Start,End\n";
        ResponseEntity<ByteArrayResource> response = restTemplate.getForEntity("/employees?names=this training doesn't exist", ByteArrayResource.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(new String(Objects.requireNonNull(response.getBody()).getByteArray(), StandardCharsets.UTF_8))
                .isEqualTo(reportTitle);
    }

    @Test
    public void getTrainingsReport_ShouldReturnOk_WhenTrainingsListIsNotEmpty() {
        String reportTitle = "Training name,Location,Trainer,Skills,Discipline,Start,End\n";
        ResponseEntity<ByteArrayResource> response = restTemplate.getForEntity("/trainings?discipline=Development", ByteArrayResource.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(new String(Objects.requireNonNull(response.getBody()).getByteArray(), StandardCharsets.UTF_8))
                .isNotEqualTo(reportTitle);
    }

    @Test
    public void getTrainingsReport_ShouldReturnOk_WhenTrainingsListIsEmpty() {
        String reportTitle = "Training name,Location,Trainer,Skills,Discipline,Start,End\n";
        ResponseEntity<ByteArrayResource> response = restTemplate.getForEntity("/trainings?names=this training doesn't exist", ByteArrayResource.class);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(new String(Objects.requireNonNull(response.getBody()).getByteArray(), StandardCharsets.UTF_8))
                .isEqualTo(reportTitle);
    }
}
