package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.dto.TrainingDto;
import org.assertj.core.api.Assertions;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TrainingControllerIT {
    private static RestTemplate restTemplate;
    @Autowired
    private Flyway flyway;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:8080/trainings"));
    }

    @BeforeEach
    public void cleanup() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void create_shouldReturnCreated_whenTrainingIsValid() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("valid_training.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<TrainingDto> responseEntity = restTemplate.exchange("/", HttpMethod.POST, requestEntity, TrainingDto.class);
        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    public void create_shouldReturnBadRequest_whenTrainingHasNoName() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("no_name_training.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/", HttpMethod.POST, requestEntity, TrainingDto.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void create_shouldReturnBadRequest_whenTrainingMeetingUrlIsInvalid() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("invalid_meeting_url_training.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/", HttpMethod.POST, requestEntity, TrainingDto.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void create_shouldReturnBadRequest_whenTrainingLocationIsInvalid() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("invalid_location_training.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/", HttpMethod.POST, requestEntity, TrainingDto.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void create_shouldReturnCreated_whenTrainingHasNoDiscipline() {
        //deprecated
    }

    @Test
    public void create_shouldReturnCreated_whenRequestHasNoIcsFile() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("valid_training.json"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<TrainingDto> responseEntity = restTemplate.exchange("/", HttpMethod.POST, requestEntity, TrainingDto.class);
        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    public void create_shouldReturnBadRequest_whenParticipantsContainsAnInvalidEmail() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("invalid_email_training.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/", HttpMethod.POST, requestEntity, TrainingDto.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void create_shouldReturnCreated_whenTrainingHasPDRDiscipline() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("pdr_training.json"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<TrainingDto> responseEntity = restTemplate.exchange("/", HttpMethod.POST, requestEntity, TrainingDto.class);
        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    public void update_shouldReturnAccepted_whenTrainingWithId1ExistsInDatabaseAndAllFieldsAreValid() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("valid_training.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);

        ResponseEntity<TrainingDto> responseEntity = restTemplate.exchange("/2", HttpMethod.PUT, requestEntity, TrainingDto.class);
        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
    }

    @Test
    public void update_shouldReturnNotFound_whenTrainingWithId400DoesNotExistInDatabase() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("valid_training.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);

        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/4000", HttpMethod.PUT, requestEntity, TrainingDto.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void update_shouldReturnBadRequest_whenTrainingHasNoName() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("no_name_training.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);

        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/1", HttpMethod.PUT, requestEntity, TrainingDto.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void getTrainingById_shouldFoundTraining_whenIdIs2() {
        ResponseEntity<TrainingDto> response = restTemplate.getForEntity("/2", TrainingDto.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        TrainingDto body = response.getBody();
        Assertions.assertThat(body).isNotNull();
        Assertions.assertThat(body.getName()).isEqualTo("First C# training");
        Assertions.assertThat(body.getLocation()).isEqualTo("Remote");
        Assertions.assertThat(body.getMeetingURL()).isEqualTo("https://random_meeting_url.ro");
    }

    @Test
    void getTrainingById_shouldReturnNotFoundTraining_whenIdIs100() {
        HttpEntity<Void> request = new HttpEntity<>(null);
        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/100000", HttpMethod.GET, request, TrainingDto.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void deleteTrainingById_shouldNoContentTraining_whenIdIs1() {
        HttpEntity<Void> request = new HttpEntity<>(null);
        ResponseEntity<Void> response = restTemplate.exchange("/1", HttpMethod.DELETE, request, Void.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    void deleteTrainingById_shouldReturnNotFoundTraining_whenIdIs100() {
        HttpEntity<Void> request = new HttpEntity<>(null);
        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/100000", HttpMethod.DELETE, request, Void.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }

    @Test
    public void getAllTrainings_shouldNotReturnAnEmptyList() {
        List<TrainingDto> trainings = restTemplate.getForObject("", List.class);
        Assertions.assertThat(trainings.size()).isNotZero();
    }

    @Test
    public void getPaginatedTrainings_WhenPageIs1_ShouldNotReturnAnEmptyList() {
        List<TrainingDto> trainings = restTemplate.getForObject("/page/1", List.class);
        Assertions.assertThat(trainings.size()).isNotZero();
    }

    @Test
    public void getPaginatedTrainings_WhenPageIsTooHigh_ShouldThrowException_AndBadRequestHttpCode() {
        HttpClientErrorException exception = Assertions.catchThrowableOfType(() -> restTemplate.getForObject("/page/100", List.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void getPaginatedTrainings_WhenPageNumberIsNegative_ShouldReturnBadRequestHttpCode() {
        HttpClientErrorException exception = Assertions.catchThrowableOfType(() -> restTemplate.getForObject("/page/-1", List.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void getNumberOfTrainings_shouldReturn57() {
        //given
        Long expectedNumberOfTrainings = 57l;

        //when
        Long actualNumberOfTrainings = restTemplate.getForObject("/numberOfTrainings", Long.class);

        //that
        Assertions.assertThat(actualNumberOfTrainings).isEqualTo(expectedNumberOfTrainings);
    }

    @Test
    public void createTraining_shouldReturnBadRequest_WhenJsonParameterIsUnknown() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("training", new ClassPathResource("training_with_unknown_field.json"));
        body.add("icsFile", new ClassPathResource("test_file.ics"));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        HttpClientErrorException exception = Assertions.catchThrowableOfType(() -> restTemplate.exchange("/", HttpMethod.POST, requestEntity, TrainingDto.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

}
