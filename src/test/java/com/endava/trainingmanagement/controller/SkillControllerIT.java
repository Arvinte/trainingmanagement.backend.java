package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.TrainingManagementApplication;
import com.endava.trainingmanagement.dto.SkillDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SkillControllerIT {
    private static RestTemplate restTemplate;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:8080/skills"));
    }

    @Test
    public void getAll_shouldNotReturnAnEmptyList() {
        List<SkillDto> skills = restTemplate.getForObject("", List.class);
        Assertions.assertThat(skills.size()).isNotZero();
    }
}
