package com.endava.trainingmanagement.controller;

import com.endava.trainingmanagement.dto.DisciplineDto;
import com.endava.trainingmanagement.dto.EmployeeDto;
import com.endava.trainingmanagement.exceptions.LoginException;
import com.endava.trainingmanagement.model.Employee;
import com.endava.trainingmanagement.service.EmployeeService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class LoginControllerIT {
    private static RestTemplate restTemplate;
    @Autowired
    private EmployeeService employeeService;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:8080"));
    }


    @Test
    void login_shouldThrownException_whenEmail1IsNotRegisteredInDatabase() {
        String expected = "The email email1@example.c is not registered in the system";
        String email = "email1@example.c";

        Throwable exception = assertThrows(LoginException.class, () -> this.employeeService.getByEmail(email));
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void login_shouldFoundEmployee_whenEmailIsEmail1() {
        String email = "email1@example.com";

        ResponseEntity<EmployeeDto> response = restTemplate.postForEntity("/login", email, EmployeeDto.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        DisciplineDto disciplineDto = new DisciplineDto();
        disciplineDto.setName("Development");
        EmployeeDto employee = response.getBody();
        Assertions.assertThat(employee).isNotNull();
        Assertions.assertThat(employee.getId()).isEqualTo(1L);
        Assertions.assertThat(employee.getFirstName()).isEqualTo("George");
        Assertions.assertThat(employee.getLastName()).isEqualTo("Enache");
        Assertions.assertThat(employee.getEmail()).isEqualTo("email1@example.com");
        Assertions.assertThat(employee.getDiscipline()).isEqualTo(disciplineDto);
    }


    @Test
    void login_shouldThrownUnauthorized_whenEmail1IsNotRegisteredInDatabase() {
        String email = "email1@example.c";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);

        HttpEntity<String> requestEntity = new HttpEntity<>(email, headers);

        HttpClientErrorException exception = Assertions.catchThrowableOfType(() ->
                restTemplate.exchange("/login", HttpMethod.POST, requestEntity, Employee.class), HttpClientErrorException.class);
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);

    }
}
