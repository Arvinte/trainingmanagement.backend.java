package com.endava.trainingmanagement.util;

import com.endava.trainingmanagement.exceptions.InvalidStringException;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static com.endava.trainingmanagement.util.StringUtils.capitalize;
import static com.endava.trainingmanagement.util.StringUtils.removeExtraWhitespaces;

public class StringUtilsTest {

    @Test
    public void capitalize_ShouldReturnCapitalizedWord_WhenReceivingAWordWithoutAdditionalWhiteSpaces() {
        //given
        String word = "test";
        String expectedWord = "Test";

        //when
        String actualWord = capitalize(word);

        //that
        Assertions.assertThat(actualWord).isEqualTo(expectedWord);
    }

    @Test
    public void capitalize_ShouldReturnCapitalizedWord_WhenReceivingAWordDividedByWhiteSpaces() {
        //given
        String word = " test ";
        String expectedWord = "Test";

        //when
        String actualWord = capitalize(word);

        //that
        Assertions.assertThat(actualWord).isEqualTo(expectedWord);
    }

    @Test
    public void capitalize_ShouldReturnCapitalizedWords_WhenReceivingMultipleWordsDividedByASpace(){
        //given
        String words = "wow nice test";
        String expectedWords = "Wow Nice Test";

        //when
        String actualWords = capitalize(words);

        //that
        Assertions.assertThat(actualWords).isEqualTo(expectedWords);
    }

    @Test
    public void capitalize_ShouldReturnCapitalizedWords_WhenReceivingMultipleWordsDividedByMultipleWhiteSpaces() {
        //given
        String words = " wow  nice   test ";
        String expectedWords = "Wow Nice Test";

        //when
        String actualWords = capitalize(words);

        //that
        Assertions.assertThat(actualWords).isEqualTo(expectedWords);
    }

    @Test(expected = InvalidStringException.class)
    public void capitalize_ShouldThrowException_WhenStringIsEmpty() {
        //given
        String words = "";

        //that
        capitalize(words);
    }

    @Test(expected = InvalidStringException.class)
    public void capitalize_ShouldThrowException_WhenStringContainsOnlyWhiteSpaces() {
        //given
        String words = "   ";

        //that
        capitalize(words);
    }

    @Test
    public void removeExtraWhitespaces_ShouldReturnSameString_WhenReceivingAWordWithoutAdditionalWhiteSpaces() {
        //given
        String word = "test";
        String expectedWord = "test";

        //when
        String actualWord = removeExtraWhitespaces(word);

        //that
        Assertions.assertThat(actualWord).isEqualTo(expectedWord);
    }

    @Test
    public void removeExtraWhitespaces_ShouldReturnWordWithoutSpaces_WhenReceivingAWordDividedByWhiteSpaces() {
        //given
        String word = " test ";
        String expectedWord = "test";

        //when
        String actualWord = removeExtraWhitespaces(word);

        //that
        Assertions.assertThat(actualWord).isEqualTo(expectedWord);
    }

    @Test
    public void removeExtraWhitespaces_ShouldRemoveExtraSpaces_WhenReceivingMultipleWordsDividedByMultipleWhiteSpaces() {
        //given
        String words = " wow  nice   test ";
        String expectedWords = "wow nice test";

        //when
        String actualWords = removeExtraWhitespaces(words);

        //that
        Assertions.assertThat(actualWords).isEqualTo(expectedWords);
    }

    @Test
    public void removeExtraWhitespaces_ShouldReturnSameString_WhenReceivingMultipleWordsDividedByASpace() {
        //given
        String words = "wow nice test";
        String expectedWords = "wow nice test";

        //when
        String actualWords = removeExtraWhitespaces(words);

        //that
        Assertions.assertThat(actualWords).isEqualTo(expectedWords);
    }
}
