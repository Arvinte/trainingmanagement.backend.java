package com.endava.trainingmanagement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class TrainingManagementApplicationTests {

    @Test
    void contextLoads() {
    }

}
